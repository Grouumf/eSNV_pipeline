#! /usr/bin/python

""" check overall statistics for all log files from fastqc report"""

from os import listdir
from os.path import isfile
from os.path import isdir

from os import makedirs


import re

from collections import Counter

from SNV_pipeline.config import PATH_OUTPUT
from SNV_pipeline.config import PATH_HTML
from SNV_pipeline.config import PROJECT_NAME

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import mpld3


PATH_HTML += '/aligners/'
PATH_OUTPUT_FASTQC = PATH_OUTPUT + '/fastqc/data/'

if not isdir(PATH_OUTPUT_FASTQC):
    makedirs(PATH_OUTPUT_FASTQC)

def main():
    make_aligner_quality_csv()
    plot_duplication_stats()

def plot_duplication_stats():
    regex_status = "(?<=Sequence Duplication Levels\t)\w+"
    regex_status = re.compile(regex_status)

    regex_stat = "(?<=Total Deduplicated Percentage\t)[0-9]+\.[0-9]+"
    regex_stat = re.compile(regex_stat)

    stats_perc = []
    stats_status = []

    for folder in listdir(PATH_OUTPUT_FASTQC):
        log_file = "{0}/{1}/fastqc_data.txt"\
                   .format(PATH_OUTPUT_FASTQC, folder)

        if not isfile(log_file):
            continue

        read = open(log_file, 'r').read()
        perc = regex_stat.findall(read)[0]
        status = regex_status.findall(read)[0]

        stats_perc.append(float(perc))
        stats_status.append(status)

    fig, axes = plt.subplots(2, 1, figsize=(12, 12))

    ax = axes[0]
    ax.boxplot(stats_perc)
    ax.set_ylabel('total deduplicated percentage')

    ax = axes[1]
    count = Counter(stats_status)
    ax.pie(count.values(),
           labels=count.keys(),
           shadow=True,
           autopct='%1.1f%%')
    ax.set_ylabel('status percentage')

    mpld3.save_html(fig,
                    PATH_HTML + '{0}_deduplicated_sequences.html'\
                    .format(PROJECT_NAME))

def make_aligner_quality_csv():
    """ """
    regex_status = "(?<=Sequence Duplication Levels\t)\w+"
    regex_status = re.compile(regex_status)

    stats_status = {}

    for folder in listdir(PATH_OUTPUT_FASTQC):
        log_file = "{0}/{1}/fastqc_data.txt"\
                   .format(PATH_OUTPUT_FASTQC, folder)

        if not isfile(log_file):
            continue

        read = open(log_file, 'r').read()
        status = regex_status.findall(read)[0]
        sample = folder.rsplit('_fastqc', 1)[0]
        stats_status[sample] = status

    f_csv = open(PATH_OUTPUT + '/deduplicated_check.csv', 'w')

    for key in stats_status:
        f_csv.write('{0};{1}\n'.format(key, stats_status[key]))


if __name__ == "__main__":
    main()
