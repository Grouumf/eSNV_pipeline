source ~/.SNV_project_root.sh

SOFT_NAME=`echo ${1}|perl -pe 's/.soft//g'`

python2.7 ${CODE_ROOT}/SNV_pipeline/SNV_pipeline/server/process_soft.py --soft_file ${SOFT_NAME}.soft

echo "GSE processed: "$SOFT_NAME

max_simultaneous_array=${MAX_SIMULTANEOUS_ARRAY}

start_size=0

FILE=0

for file in `ls ${PATH_OUTPUT_FOLDER}/${SOFT_NAME}/gsm_to_download*.tsv`; do
    size=`cat ${file}|wc -l`
    size=$(( $size - 1 ))
    cmd="sbatch --array=0-${size}%${max_simultaneous_array} --job-name ${SOFT_NAME} ${CODE_ROOT}/SNV_pipeline/SNV_pipeline/slurm/process_everything_to_infer_eeSNVs.slurm ${file} ${@: 2}"
    echo "command launched: ${cmd}"
    ${cmd}
    FILE=$(( $FILE + 1 ))

done;
