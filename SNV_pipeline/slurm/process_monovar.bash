source ~/.bash_variable_${1}.sh

DATA_PATH=$OUTPUT_ROOT/data/$PROJECT_NAME/
PATH_CODE=$CODE_ROOT

folders=(`cat ${PATH_MONOVAR_REGION}`)
size=${#folders[@]}
max_simultaneous_array=30

sbatch --array=0-${size}%${max_simultaneous_array}  ${PATH_CODE}/SNV_pipeline/SNV_pipeline/slurm/process_monovar.slurm ${PROJECT_NAME}

echo "slurm task for monovar and project ${PROJECT_NAME} launched!"
