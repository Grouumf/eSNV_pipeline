
source ~/.bash_variable_${1}.sh

DATA_PATH=$FASTQ_PATH
PATH_CODE=$CODE_ROOT

size=0

for folder in `ls ${DATA_PATH}/`;do
    if [[ $folder != *'.sra' ]]
       then
           ((size++))
    fi
done

max_simultaneous_array=30

sbatch --array=0-${size}%${max_simultaneous_array} ${PATH_CODE}/SNV_pipeline/SNV_pipeline/slurm/process_everything.slurm ${PROJECT_NAME}
