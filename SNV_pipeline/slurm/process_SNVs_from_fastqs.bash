source ~/.SNV_project_root.sh

FASTQS_FOLDER=${1}
ORGANISM=${2}
PROJECT_NAME=${3}

python2.7 ${CODE_ROOT}/SNV_pipeline/SNV_pipeline/server/process_fastq_folder.py \
          --fastqs_folder $FASTQS_FOLDER \
          --organism $ORGANISM \
          --project_name $PROJECT_NAME

echo "fastq folder processed: "$FASTQ

max_simultaneous_array=${MAX_SIMULTANEOUS_ARRAY}

FILE=0

for file in `ls ${FASTQS_FOLDER}/fastq_folder_batch*.tsv`; do
    size=`cat ${file}|wc -l`
    size=$(( $size - 1 ))
    cmd="sbatch --array=0-${size}%${max_simultaneous_array} --job-name ${PROJECT_NAME} ${CODE_ROOT}/SNV_pipeline/SNV_pipeline/slurm/process_SNVs_from_fastqs.slurm ${file} ${@: 4}"
    echo "command launched: ${cmd}"
    ${cmd}
    FILE=$(( $FILE + 1 ))

done;
