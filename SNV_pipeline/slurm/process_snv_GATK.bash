source ~/.bash_variable_${1}.sh

DATA_PATH=$OUTPUT_ROOT/data/$PROJECT_NAME/
PATH_CODE=$CODE_ROOT

folders=(`ls ${DATA_PATH}/star`)
size=${#folders[@]}
max_simultaneous_array=35
echo $PATH_CODE

sbatch --array=0-${size}%${max_simultaneous_array} ${PATH_CODE}/SNV_pipeline/SNV_pipeline/slurm/process_snv_travers.slurm ${PROJECT_NAME}
