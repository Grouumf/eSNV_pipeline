#! /usr/bin/python

""" process one SSR with eSNV"""

from os import popen
from os.path import isdir
from os.path import isfile

from distutils.dir_util import mkpath
from shutil import rmtree
from shutil import copyfile
from sys import stdout as sys_stdout
from sys import argv
from random import randint
from random import random
from time import sleep

############ VARIABLES ############################################
SRR_TO_PROCESS = "GSM1835508__SRX1123146__SRR2132768" # for debug purpose
PROCESS_ID = randint(0, 1000000)

if "--specific_folder" in argv:
    SRR_TO_PROCESS = argv[
        argv.index("--specific_folder") + 1]
if "--process_id" in argv:
    PROCESS_ID = argv[
        argv.index("--process_id") + 1]
###################################################################


from SNV_pipeline.config import OUTPUT_PATH_ESNV as CURRENT_PATH
from SNV_pipeline.config import PATH_TO_DATA
from SNV_pipeline.config import ESNV_PATH
from SNV_pipeline.config import ESNV_CONFIG_PATH
from SNV_pipeline.mk_config_file import make_config


def main():
    process_esnv = Process_eSNV(id=PROCESS_ID)
    process_esnv.process()

class Process_eSNV():
    """ """
    def __init__(self,
                 current_path=CURRENT_PATH,
                 path_to_data=PATH_TO_DATA,
                 id="1",
                 esnv_path=ESNV_PATH):
        self.current_path = current_path
        self.path_to_data = PATH_TO_DATA
        self.esnv_path = esnv_path
        self.id = str(id)

    def process(self, srr_to_process=SRR_TO_PROCESS):
        """ process one srr"""
        tmppath = self.current_path + "/tmp/" + self.id

        sleep(random())
        if not isdir(tmppath):
            mkpath(tmppath)

        popen("rm {0}/*".format(tmppath)).read()
        make_config(tmppath + '/config.txt')
        stdout = open(tmppath + '/stdout.log', 'a+')
        stdout.write('\n#### file id {0} ####\n'.format(srr_to_process))
        tpath = self.path_to_data + "/tophat/"  + srr_to_process
        mpath = self.path_to_data + "/mapsplice/"  + srr_to_process

        if not isfile(tpath + '/accepted_hits.bam'):
            print('error file : {0} not found!'\
                .format(tpath + '/accepted_hits.bam'))
            return

        if not isfile(mpath + '/alignments.bam'):
            print('error file : {0} not found!'\
                .format(mpath + '/alignments.bam'))
            return

        copyfile("{0}/accepted_hits.bam".format(tpath),
                 "{0}/accepted_hits.bam".format(tmppath))

        copyfile("{0}/alignments.bam".format(mpath),
                 "{0}/alignments.bam".format(tmppath))

        cmd = "bash {0}/src/main.sh {2} {1}" \
              " accepted_hits.bam:alignments.bam" \
              " {3}/data/{1} {2}/config.txt" \
              .format(self.esnv_path,
                      srr_to_process,
                      tmppath,
                      self.current_path)

        runf = popen(cmd)
        c = runf.read(1)
        buff_size = 0

        while c:
            stdout.write(c)
            stdout.flush()
            c = runf.read(1)
            sys_stdout.write(c)
            buff_size += 1

            if buff_size > 5:
                sys_stdout.flush()
                buff_size = 0
        popen("rm -r {0}/*.bam".format(tmppath)).read()

if __name__ == "__main__":
    main()
