"""prepare global variables for bash sbatch scripts """

from os import getenv

from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD
from SNV_pipeline.server.server_config import CODE_ROOT
from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER
from SNV_pipeline.server.server_config import MAX_SIMULTANEOUS_ARRAY
from SNV_pipeline.server.server_config import PATH_LOCAL_DATA


def main():
    pathfile = '{0}/.SNV_project_root.sh'.format(getenv("HOME"))

    with open(pathfile, 'w') as f:
        f.write('SOFT_FILE_TO_DOWNLOAD={0}\n'.format(SOFT_FILE_TO_DOWNLOAD))
        f.write('PATH_OUTPUT_FOLDER={0}\n'.format(PATH_OUTPUT_FOLDER))
        f.write('CODE_ROOT={0}\n'.format(CODE_ROOT))
        f.write('MAX_SIMULTANEOUS_ARRAY={0}\n'.format(MAX_SIMULTANEOUS_ARRAY))
        f.write('PATH_LOCAL_DATA={0}\n'.format(PATH_LOCAL_DATA))


if __name__ == "__main__":
    main()
