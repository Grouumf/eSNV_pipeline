from SNV_pipeline.process_snv_GATK import ProcessGATKSNV

from os.path import getsize

from os.path import isfile
from os.path import isdir
from os import popen

from shutil import copyfile

from sys import argv

from distutils.dir_util import mkpath


if "--python" in argv:
    PYTHON = argv[
        argv.index("--python") + 1]
else:
    from SNV_pipeline.config import PYTHON


if "--prog_root" in argv:
    PROG_ROOT = argv[
        argv.index("--prog_root") + 1]
else:
    from SNV_pipeline.config import PROG_ROOT


def main():
    """ """
    process_allelic_counter = ProcessAllelicCounter()
    process_allelic_counter.process()


class ProcessAllelicCounter(ProcessGATKSNV):
    """
    """
    def __init__(self,
                 python_path=PYTHON,
                 prog_root=PROG_ROOT,
                 **kwargs):
        """
        """
        self.python_path = python_path
        self.prog_root = prog_root

        ProcessGATKSNV.__init__(self, **kwargs)

    def process(self, srr_to_process=None):
        """
        """
        if srr_to_process:
            self.srr_to_process = srr_to_process

        self._init_process()
        # self._launch_picard_readgroups()
        # self._launch_picard_markduplicates()
        # self._launch_gatk_cigar()
        # self._launch_gatk_asereadcounter()
        self._launch_python_phaser()
        self._finish_process()

    def _init_process(self):
        """ """
        self.tmppath = '{0}/data/{1}/tmp/'.format(self.output_path,  self.srr_to_process)
        self.respath = '{0}/data/{1}'.format(self.output_path,  self.srr_to_process)

        if not isdir(self.tmppath):
            mkpath(self.tmppath)

        self.stdout = open(self.tmppath + '/stdout.log', 'a+')

        self.stdout.write('\n\n######## allelic counter file id {0} ########\n'\
                          .format(self.srr_to_process))

        if not self.bam_file_name:
            self.bam_file_path = '{0}{1}/Aligned.sortedByCoord.out.bam'.format(
                self.path_to_data +  "/star/" ,  self.srr_to_process)
        else:
            self.bam_file_path = self.path_to_data + self.bam_file_name

        if not isfile(self.bam_file_path)\
                or not getsize(self.bam_file_path):
            err = 'error file : {0} not found or empty!'\
                .format(self.bam_file_path)
            raise Exception(err)

        copyfile("{0}".format(self.bam_file_path),
                 "{0}/Aligned.sortedByCoord.out.bam".format(self.tmppath))


    def _finish_process(self):
        """mk res folders... """
        pass

    def _launch_gatk_asereadcounter(self):
        """
        Running Realignment
        """
        popen("rm {0}/test_allele.test".format(self.tmppath)).read()
        self._run_cmd(
            'echo "\n\n######## LAUNCHING ASEReadCounter ########\n"')

        cmd = "{0} {1} -jar {2}/GenomeAnalysisTK.jar -T ASEReadCounter" \
        " -I {3}/split.bam" \
        " --out {3}/test_allele.test" \
        " -R {4}" \
        " -sites {5}/data/{6}/snv_filtered.vcf" \
        " -U ALLOW_N_CIGAR_READS" \
        .format(self.java,
                self.java_mem,
                self.gatk_dir,
                self.tmppath,
                self.ref_genome,
                self.output_path,
                self.srr_to_process,
              )

        self._run_cmd(cmd)

    def _launch_python_phaser(self):
        """
        """
        popen(' rm {0}/snv_gziped.vcf.gz'.format(self.tmppath)).read()
        copyfile("{0}/data/{1}/snv_filtered.vcf".format(self.output_path,
                                                        self.srr_to_process)
                 , '{0}/snv_gziped.vcf'.format(self.tmppath))

        self._run_cmd('bgzip {0}/snv_gziped.vcf'.format(self.tmppath))
        self._run_cmd('tabix {0}/snv_gziped.vcf.gz'.format(self.tmppath))

        self._run_cmd(
            'echo "\n\n######## LAUNCHING python pHASER ########\n"')

        cmd = "{0} {1}/phaser/phaser/phaser.py" \
              " --bam {2}/split.bam " \
              " --vcf {2}/snv_gziped.vcf.gz"\
              " --sample 0 --o {2}/phase_out" \
              " --mapq 0 --baseq 0 --paired_end 1".format(
                  self.python_path,
                  self.prog_root,
                  self.tmppath,
                  self.srr_to_process)

        self._run_cmd(cmd)



if __name__ == '__main__':
    main()
