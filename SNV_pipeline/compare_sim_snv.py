from SNV_pipeline.config import OUTPUT_PATH_STAR
from SNV_pipeline.config import MUTATION_FILE

from glob import glob

from os import popen

from multiprocessing.pool import Pool


######################## VARIABLE ############################
BEDTOOLS = 'bedtools'
NB_THREADS = 10
##############################################################


def main():
    """
    """
    create_sim_snv_intersection_with_bam()

def create_sim_snv_intersection_with_bam():
    """
    """
    pool = Pool(NB_THREADS)
    print('#### procssing individual mutation file...')
    map(_multiprocess_bed, glob('{0}/*'.format(OUTPUT_PATH_STAR)))

def _multiprocess_bed(folder):
    """
    """
    create_mutation_file(folder)
    create_bedfile_for_bam(folder)
    compute_snv_intersection(folder)

def create_mutation_file(folder):
    """
    """
    f_mut = open('{0}/sim_mutation.bed'.format(folder), 'w')
    f_ref = open(MUTATION_FILE, 'r')

    for line in f_ref:
        line = line.strip('\n').split('\t')

        if len(line) == 5:
            f_mut.write('\t'.join(line)+ '\n')
        else:
            f_mut.write('{0}\t{1}\t{1}\t{2}\t{3}\n'.format(*line))

def create_bedfile_for_bam(folder):
    """ """
    bam_file = folder + '/Aligned.sortedByCoord.out.bam'
    bed_file = folder + '/Aligned_reads.bed'

    cmd = '{0} bamtobed -i {1} > {2}'.format(BEDTOOLS, bam_file, bed_file)
    popen(cmd).read()

def compute_snv_intersection(folder):
    """
    """
    snv_file = folder + '/sim_mutation.bed'
    bed_file = folder + '/Aligned_reads.bed'

    cmd = 'bedtools intersect -a {0} -b {1} -c > {2}/snv_intersect.bed'.format(
        snv_file, bed_file, folder)

    popen(cmd).read()

if __name__ == "__main__":
    main()
