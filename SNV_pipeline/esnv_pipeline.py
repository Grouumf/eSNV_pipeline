#!/usr/bin/python

import sys

from os import popen
from os import listdir
from os import mkdir

from os.path import isdir
from distutils.dir_util import mkpath

INPUT = ["SRR1295124"]
PATH_ALIGNER1 = "/home/opoirion/data/glioblastoma/mapsplice/"
PATH_ALIGNER2 = "/home/opoirion/data/glioblastoma/tophat/"

OUTPUT_PATH = "/home/opoirion/data/esnv/"


if not isdir(OUTPUT_PATH):
    mkpath(OUTPUT_PATH)

def main():
    for inpt in INPUT:
        print("====> bam folders code:", inpt)
        if not isdir(OUTPUT_PATH + inpt):
            mkdir(OUTPUT_PATH + inpt)
        assert()

if __name__ == "__main__":
    main()
