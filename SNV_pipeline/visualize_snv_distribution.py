#! /usr/bin/python

"""
Visualize SNV positionning distribution as histogram using matplotlib
"""

from os import listdir
from os.path import isfile
from os.path import isdir

import re
from collections import defaultdict

from SNV_pipeline.config import PATH_HTML
from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import OUTPUT_PATH_GATK

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import mpld3


############ LOCAL VARIABLES ################
PATH_HTML += '/snv_qc/'
BIN_SIZE = 300
COLUMN_NUMBER = 3
#############################################


def main():
    snv_dict = load_snv_features_from_all_files()
    genome_dict = load_genome_features()
    plot_snv_distribution(snv_dict, genome_dict)

def plot_snv_distribution(snv_dict, genome_dict):
    """ """
    row_num = len(snv_dict) / COLUMN_NUMBER
    fig, axes = plt.subplots(row_num + 1, COLUMN_NUMBER, figsize=(30, 30))
    line_nb = 0
    col_nb = 0

    for chr_id in snv_dict:
        ax = axes[line_nb][col_nb]
        ax.set_xlim([0, genome_dict[chr_id]])
        ax.hist(snv_dict[chr_id], BIN_SIZE)
        ax.set_xlabel(chr_id)

        col_nb += 1

        if col_nb >= COLUMN_NUMBER:
            col_nb = 0
            line_nb += 1

    mpld3.save_html(fig, PATH_HTML + 'snv_dist.html')
    print "fig saved at:{0}".format(PATH_HTML + 'snv_dist.html')

def load_genome_features():
    """ """
    genome_dict_path = REF_GENOME.rsplit('.', -1)[0] +'.dict'
    genome_dict = {}

    with open(genome_dict_path, 'r') as f:
        f.readline()
        for line in f:
            line = line.split('\t')
            genome_dict[line[1].split(':')[-1]] = int(line[2].split(':')[-1])
    return genome_dict

def load_snv_features_from_file(snv_path, snv_dict):
    """ """
    f_snv = open(snv_path, 'r')

    for line in f_snv:
        if line[0] == '#':
            continue
        line = line.split('\t')

        # process only passed SNV
        if line[6] != 'PASS':
            continue

        snv_dict[line[0]].append(int(line[1]))

def load_snv_features_from_all_files():
    """ """
    snv_dict = defaultdict(list)
    output_path_esnv = OUTPUT_PATH_GATK + '/data/'

    for folder in listdir(output_path_esnv):
        if not isdir(output_path_esnv):
            continue

        snv_path = "{0}/{1}/snv_filtered.vcf".format(output_path_esnv, folder)

        if not isfile(snv_path):
            continue

        load_snv_features_from_file(snv_path, snv_dict)

    return snv_dict



if __name__ == "__main__":
    main()
