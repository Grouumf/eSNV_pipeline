#!/usr/bin/python

import sys
from fnmatch import fnmatch

from os import popen
from os import listdir
from os import mkdir
from os.path import isdir
from os.path import isfile
from time import sleep
from random import random
from sys import argv

from distutils.dir_util import mkpath

from SNV_pipeline.config import FASTQ_PATH
from SNV_pipeline.config import OUTPUT_PATH_TOPHAT \
    as OUTPUT_PATH
from SNV_pipeline.config import BOWTIE2
from SNV_pipeline.config import TOPHAT_OPTION
from SNV_pipeline.config import TOPHAT_THREADS as THREADS
from SNV_pipeline.config import SPECIFIC_FILENAME_PATTERN as PATTERN
from SNV_pipeline.config import PATH_TOPHAT_SOFTWARE as PATH_SOFTWARE


if not isdir(OUTPUT_PATH):
    sleep(2 * random())
    mkpath(OUTPUT_PATH)


############ VARIABLES ############################################
if "--specific_folder" in argv:
    PATTERN = argv[
        argv.index("--specific_folder") + 1]
###################################################################


def main():
    for fil in listdir(FASTQ_PATH):

        if isfile(FASTQ_PATH + fil):
            continue

        if PATTERN and not fnmatch(fil, PATTERN):
            continue

        print("====> file to be aligned:", fil)

        if not isdir(OUTPUT_PATH + fil):
            mkdir(OUTPUT_PATH + fil)

        if isfile(OUTPUT_PATH + fil + "/accepted_hits.bam"):
            print('bam file result alreay exists for:{0}\nskipping...'\
                .format(fil))
            continue

        fastq_str = ""
        for fastq_fil in listdir(FASTQ_PATH + fil):
            print(fastq_fil)
            if fnmatch(fastq_fil, "*.fastq"):
                fastq_str += "{0}{1}/{2} ".format(FASTQ_PATH, fil, fastq_fil)

        if not fastq_str:
            print('no fastq file found for:{0}!\nskipping'.format(fil))
            continue

        cmd = "{6} " \
              " -p {4} {5} -o {2}{0} " \
              "{1} {3}".format(fil,
                               BOWTIE2,
                               OUTPUT_PATH,
                               fastq_str,
                               THREADS,
                               TOPHAT_OPTION,
                               PATH_SOFTWARE)
        popen(cmd).read()


if __name__ == "__main__":
    main()
