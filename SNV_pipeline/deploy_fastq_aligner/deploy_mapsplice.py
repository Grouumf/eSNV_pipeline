#!/usr/bin/python

import sys
from fnmatch import fnmatch

from os import popen
from os import listdir
from os import mkdir
from os.path import isdir
from os.path import isfile
from time import sleep
from random import random
from sys import argv

from distutils.dir_util import mkpath

from SNV_pipeline.config import FASTQ_PATH
from SNV_pipeline.config import OUTPUT_PATH_MAPSPLICE \
    as OUTPUT_PATH
from SNV_pipeline.config import BOWTIE
from SNV_pipeline.config import GENOME_PATH
from SNV_pipeline.config import PATH_MAPSPLICE_SOFTWARE \
    as PATH_SOFTWARE
from SNV_pipeline.config import MAPSPLICE_THREADS as THREADS
from SNV_pipeline.config import SPECIFIC_FILENAME_PATTERN as PATTERN
from SNV_pipeline.config import MAPSPLICE_OPTION


if not isdir(OUTPUT_PATH):
    sleep(2 * random())
    mkpath(OUTPUT_PATH)


############ VARIABLES ############################################
if "--specific_folder" in argv:
    PATTERN = argv[
        argv.index("--specific_folder") + 1]
###################################################################


def main():
    for fil in listdir(FASTQ_PATH):

        if isfile(FASTQ_PATH + fil):
            continue

        if PATTERN and not fnmatch(fil, PATTERN):
            continue

        print("====> file to be aligned:", fil)

        if not isdir(OUTPUT_PATH + fil):
            mkdir(OUTPUT_PATH + fil)

        if isfile(OUTPUT_PATH + fil + "/alignments.bam"):
            print 'bam file result alreay exists for:{0}\nskipping...'\
                .format(fil)
            continue

        fastq_str = ""
        for fastq_fil in listdir(FASTQ_PATH + fil):
            print(fastq_fil)
            if fnmatch(fastq_fil, "*.fastq"):
                fastq_str += "{0}{1}/{2} ".format(FASTQ_PATH, fil, fastq_fil)

        if not fastq_str:
            print('no fastq file found for:{0}!\nskipping'.format(fil))
            continue

        cmd = "python {3} -o {4}{1} -c {0} " \
              "-1 {6}" \
              " -x {2} {7} -p {5} --bam" \
                  .format(GENOME_PATH,
                          fil,
                          BOWTIE,
                          PATH_SOFTWARE,
                          OUTPUT_PATH,
                          THREADS,
                          fastq_str,
                          MAPSPLICE_OPTION)
        popen(cmd).read()


if __name__ == "__main__":
    main()
