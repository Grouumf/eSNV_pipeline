#! /usr/bin/python

""" check overall statistics for all log files from star aligner"""

from os import listdir
from os.path import isfile
import re

from SNV_pipeline.config import OUTPUT_PATH_STAR
from SNV_pipeline.config import PATH_OUTPUT
from SNV_pipeline.config import PATH_HTML
from SNV_pipeline.config import PROJECT_NAME

PATH_HTML += '/aligners/'

import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
import mpld3

import numpy as np


def main():
    make_aligner_quality_csv()
    overall_unique_reads_stats()

def overall_unique_reads_stats(
        output_path_star=OUTPUT_PATH_STAR,
        project_name=PROJECT_NAME,
        save_fig=False):
    regex = "(?<=Uniquely mapped reads \% \|\t)[0-9]+\.[0-9]+"
    regex = re.compile(regex)

    regex_read = "(?<=Number of input reads \|\t)[0-9]+"
    regex_read = re.compile(regex_read)

    regex_unmapped_short = "(?<=unmapped: too many mismatches \|\t)[0-9]+"
    regex_unmapped_short = re.compile(regex_unmapped_short)

    regex_unmapped_other = "(?<=unmapped: too short \|\t)[0-9]+"
    regex_unmapped_other = re.compile(regex_unmapped_other)

    regex_unmapped_mism = "(?<=unmapped: other \|\t)[0-9]+"
    regex_unmapped_mism = re.compile(regex_unmapped_mism)

    stats = []
    input_reads = []

    unmapped_short = []
    unmapped_other = []
    unmapped_mism = []


    for folder in listdir(output_path_star):
        log_file = "{0}/{1}/Log.final.out"\
                   .format(output_path_star, folder)

        if not isfile(log_file):
            continue

        stat = regex.findall(open(log_file, 'r').read())[0]
        read = regex_read.findall(open(log_file, 'r').read())[0]

        unmapped_short.append(float(regex_unmapped_short.findall(
            open(log_file, 'r').read())[0]))
        unmapped_other.append(float(regex_unmapped_other.findall(
            open(log_file, 'r').read())[0]))
        unmapped_mism.append(float(regex_unmapped_mism.findall(
            open(log_file, 'r').read())[0]))

        stats.append(float(stat))
        input_reads.append(int(read))

    fig, axes = plt.subplots(2, 1, figsize=(12, 12))

    print('average (std) percent of read mapped: {0} ({1})'.format(
        np.mean(stats), np.std(stats)))
    print('average (std) number of input reads: {0} ({1})'.format(
        np.mean(input_reads), np.std(input_reads)))
    print('average (std) (perc) of unmapped short reads: {0} ({1})'.format(
        np.mean(unmapped_short), np.std(unmapped_short)))
    print('average (std) (perc) of unmapped too many mismatches  reads: {0} ({1})'.format(
        np.mean(unmapped_mism), np.std(unmapped_mism)))
    print('average (std) (perc) of unmapped other reads: {0} ({1})'.format(
        np.mean(unmapped_other), np.std(unmapped_other)))

    unmapped_total = np.array(unmapped_other) + np.array(unmapped_mism) + np.array(unmapped_short)

    print('total average (std) (perc) of unmapped other reads: {0} ({1})'.format(
        np.mean(unmapped_total), np.std(unmapped_total)))

    ax = axes[0]

    ax.boxplot(stats)
    ax.set_ylabel('unique reads percent')

    ax = axes[1]

    ax.boxplot(input_reads)
    ax.set_ylabel('input reads number')

    if save_fig:
        mpld3.save_html(fig,
                        PATH_HTML + '{0}_unique_reads.html'\
                        .format(project_name))

def make_aligner_quality_csv(
        path_output=PATH_OUTPUT,
        output_path_star=OUTPUT_PATH_STAR):
    regex = "(?<=Uniquely mapped reads \% \|\t)[0-9]+\.[0-9]+"
    regex = re.compile(regex)

    regex_read = "(?<=Number of input reads \|\t)[0-9]+"
    regex_read = re.compile(regex_read)

    stats = {}
    stats_read = {}

    for folder in listdir(output_path_star):
        log_file = "{0}/{1}/Log.final.out"\
                   .format(output_path_star, folder)

        if not isfile(log_file):
            continue

        stats[folder] = regex.findall(
            open(log_file, 'r').read())[0]

        stats_read[folder] = regex_read.findall(
            open(log_file, 'r').read())[0]

    f_csv = open(path_output + '/aligner_unique_read.csv', 'w')
    f_csv_nb = open(path_output + '/aligner_unique_read_nb.csv', 'w')

    for key in stats:
        f_csv.write('{0};{1}\n'.format(key, stats[key]))

    for key in stats_read:
        f_csv_nb.write('{0};{1}\n'.format(key, stats_read[key]))


if __name__ == "__main__":
    main()
