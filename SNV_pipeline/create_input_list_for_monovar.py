from random import sample

from os import remove

from config import OUTPUT_PATH_STAR
from config import PATH_MONOVAR
from config import CHUNCK_SIZE
from config import GENOME_PATH
from config import PATH_MONOVAR_REGION
from config import PATH_MONOVAR_INPUT
from config import CHR_CHUNK_SIZE

from os.path import isdir

from glob import glob

from os import mkdir
from os import popen
from os.path import split as pathsplit

from time import sleep


if not isdir(PATH_MONOVAR):
    mkdir(PATH_MONOVAR)
    sleep(0.5)


def main():
    """ """
    create_input_region_list()
    create_list_file()

def create_input_region_list():
    """

    """
    f_region = open(PATH_MONOVAR_REGION, 'w')
    chr_sizes = get_chr_sizes()

    for chrm in chr_sizes:
        size = chr_sizes[chrm]

        count = 0

        while count < size:
            count_1 = count + CHR_CHUNK_SIZE

            if count_1 > size:
                count_1 = size

            f_region.write('{0}:{1}-{2}\n'.format(chrm, count, count_1))
            count = count_1 + 1

def get_chr_sizes():
    """
    """
    chr_size = {}

    for fil in glob('{0}/chr*.fa*'.format(GENOME_PATH)):
        chrname = pathsplit(fil)[1].rsplit('.')[0]
        nb_char = int(popen('du -b {0}'.format(fil)).read().split()[0])
        nb_char -= (len(chrname) + 2)

        chr_size[chrname] = nb_char

    return chr_size

def create_list_file():
    """
    create the input file used by monovar containing all the input files
    """
    for fil in glob('{0}/monovar_input_nb*'.format(PATH_MONOVAR)):
        remove(fil)

    file_list = set()

    for folder in glob('{0}/*'.format(OUTPUT_PATH_STAR)):
        if not isdir(folder):
            continue

        file_list.add('{0}/Aligned.sortedByCoord.out.bam'.format(folder))

    nb_file = 0

    chunck_size = CHUNCK_SIZE

    if not CHUNCK_SIZE:
        f_input = open('{0}'.format(PATH_MONOVAR_INPUT), 'w')

        for fil in file_list:
            f_input.write('{0}\n'.format(fil))

        return

    while file_list:

        if len(file_list) < chunck_size:
            chunck_size = len(file_list)

        sample_list = sample(file_list, chunck_size)
        file_list = file_list.difference(sample_list)

        f_input = open('{0}/monovar_input_nb{1}.txt'.format(PATH_MONOVAR, nb_file), 'w')

        for fil in sample_list:
            f_input.write('{0}\n'.format(fil))

            nb_file += 1

if __name__ == '__main__':
    main()
