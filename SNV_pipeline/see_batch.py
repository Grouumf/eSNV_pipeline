""" see batch ID from the fastq files thanks to the batch ID identifier """

from SNV_pipeline.config import FASTQ_PATH

from SNV_pipeline.config import GLOBAL_DATA_ROOT
from SNV_pipeline.config import PROJECT_NAME

from collections import defaultdict

from os.path import isdir
from os import listdir

from fnmatch import fnmatch


def main():
    """ """
    fastq_analyze = FastqAnalyze()
    fastq_analyze.collect_batches()
    fastq_analyze.write_csv()
    fastq_analyze.annotate_soft()

class FastqAnalyze():
    """ """
    def __init__(self):
        """ """
        self.batch_dict = defaultdict(list)
        self.batch_dict_gsm = defaultdict(list)
        self.fastq_id = self._load_csv_files()

    def _load_csv_files(self):
        """ """
        csv = '{0}/{1}/{1}.csv'.format(GLOBAL_DATA_ROOT, PROJECT_NAME)
        with open(csv, 'r') as f_csv:
            return dict([line.strip('\n').split(';')
                         for line in f_csv])

    def write_csv(self):
        """ """
        fname = '{0}/{1}/{1}_batch.res'.format(GLOBAL_DATA_ROOT, PROJECT_NAME)
        f_res = open(fname, 'w')

        f_res.write('{0} different batches founds:\n'.format(len(self.batch_dict)))

        for key in self.batch_dict:
            f_res.write('{0}\n'.format(key))

        f_res.write('\n')

        for key in self.batch_dict:
            f_res.write('\n#### {0}:\n'.format(key))

            for cell_id in self.batch_dict[key]:
                f_res.write('\t{0}\n'.format(cell_id))


    def collect_batches(self):
        """" """
        for folder in listdir(FASTQ_PATH):
            fastq_folder = FASTQ_PATH + folder

            if not isdir(fastq_folder):
                continue

            for fastq in listdir(fastq_folder):
                if fnmatch(fastq, '*.fastq'):
                    fastq_file = '{0}/{1}'.format(fastq_folder, fastq)
                    self.analyze_fastq_file(fastq_file, folder)
                    break


    def analyze_fastq_file(self, fastq_f, folder):
        """ """
        gsm = folder.split('__')[0]
        cell_id = self.fastq_id[gsm]
        f = open(fastq_f)

        line = f.readline()
        machine_id = line.split()[1].strip().split(':', 4)[:4]

        self.batch_dict[tuple(machine_id)].append(cell_id)
        self.batch_dict_gsm[tuple(machine_id)].append(gsm)

    def annotate_soft(self):
        """ """

        rep = raw_input('Do you want to annotate .soft file with the batch ID? (Y/n)')

        if rep != 'Y':
            return

        soft = open('{0}/{1}/{1}.soft'.format(GLOBAL_DATA_ROOT, PROJECT_NAME)).read()

        for batch_id, batch in enumerate(self.batch_dict_gsm.keys()):

            for gsm in self.batch_dict_gsm[batch]:
                soft = soft.replace('^SAMPLE = {0}\n'.format(gsm),
                                    '^SAMPLE = {0}\nBATCH_ID={1}\n'.format(gsm, batch_id))

        with open('{0}/{1}/{1}.soft'.format(GLOBAL_DATA_ROOT, PROJECT_NAME), 'w') as f_soft:
            f_soft.write(soft)




if __name__ == "__main__":
    main()
