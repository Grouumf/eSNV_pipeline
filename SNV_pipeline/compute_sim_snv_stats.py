from SNV_pipeline.config import OUTPUT_PATH_GATK
from SNV_pipeline.config import OUTPUT_PATH_FREEBAYES
from SNV_pipeline.config import OUTPUT_PATH_STAR
from SNV_pipeline.config import PROJECT_ROOT
from SNV_pipeline.config import PATH_OUTPUT

from SNV_pipeline.impute_snvs import SNVImputer

import numpy as np

from collections import defaultdict

from glob import glob

from os.path import isfile

from os.path import split as pathsplit

import simplejson

import re

from sys import argv

assert("--original_project_name" in argv)

ORIGINAL_PROJECT_NAME = argv[argv.index("--original_project_name") + 1]


######################## VARIABLE ############################
# ORIGINAL_PROJECT_NAME = '10x_data_original'
PATH_DATA_ORIGINAL_GATK = '{0}/data/{1}/snv_pipeline_raw/data/'.format(
    PROJECT_ROOT, ORIGINAL_PROJECT_NAME)
PATH_DATA_ORIGINAL_FREEBAYES = '{0}/data/{1}/freebayes/data/'.format(
    PROJECT_ROOT, ORIGINAL_PROJECT_NAME)
##############################################################


def main():
    """ """
    ComputeStats(caller='GATK', do_imputation=False).run()
    ComputeStats(caller='freebayes', do_imputation=False).run()

class ComputeStats():
    """
    """
    def __init__(self, caller, do_imputation=False):
        assert caller in ['GATK', 'freebayes']

        self.caller = caller
        self.nb_reads = {}
        self.snv_imputer = None

        self.snv_originals = defaultdict(list)
        self.snv_files = {}
        self.ref_snv_set_alls = {}
        self.ref_snv_sets = {}

        self.total_snvs_container = defaultdict(set)
        self.total_imputed_snvs_container = defaultdict(set)
        self.total_true_snvs_container = defaultdict(set)

        if caller == 'GATK':
            self.output_path = OUTPUT_PATH_GATK
            self.path_data_original = PATH_DATA_ORIGINAL_GATK
            self.create_snv_dict = create_snv_dict_from_GATK

        elif caller == 'freebayes':
            self.output_path = OUTPUT_PATH_FREEBAYES
            self.path_data_original = PATH_DATA_ORIGINAL_FREEBAYES
            self.create_snv_dict = create_snv_dict_from_freebayes

        self.results = {}

        if do_imputation:
            self.snv_imputer = SNVImputer(self.output_path)
            self.snv_imputer.fit()

    def run(self):
        """
        """
        for thres in range(20):
            stats = defaultdict(list)
            for folder in glob('{0}/data/*'.format(self.output_path)):

                if not  isfile('{0}/snv_filtered.vcf'.format(folder)):
                    continue

                self.iter_one_folder(folder, stats, thres)

            for key in stats:
                print('{2} {0}: {1}'.format(key, np.mean(stats[key]), self.caller))

            self.results[thres] = stats

            print('=> total ref snv detected: {0}'.format(
                len(self.total_snvs_container[thres])))
            print('=> total ref snv: {0}'.format(
                len(self.total_true_snvs_container[thres])))
            print('=> total snv imputed: {0}'.format(
                len(self.total_imputed_snvs_container[thres])))

        f_name = '{0}/sim_results_{1}.json'.format(PATH_OUTPUT, self.caller)
        open(f_name, 'w').write(
            simplejson.dumps(self.results, indent=1))
        print('data file {0} written'.format(f_name))

    def iter_one_folder(self, folder, stats, thres):
        """
        """
        gsm = pathsplit(folder)[1]
        self._create_dicts(gsm)

        ref_snv_set_all = set(self.ref_snv_set_alls[gsm].keys())
        ref_snv_set = set([key for key, count in
                           self.ref_snv_sets[gsm].iteritems() if count >= thres])

        snv_file = self.snv_files[gsm]
        snv_original = self.snv_originals[gsm]
        snv_set = snv_file.keys()

        intersection = ref_snv_set.intersection(snv_set)

        self.total_snvs_container[thres].update(intersection)
        self.total_true_snvs_container[thres].update(ref_snv_set)

        pass_snv = [snv for snv in intersection if snv_file[snv]['status'] == 'PASS']

        stats['size snv to be detected'].append(len(ref_snv_set))
        stats['size snv detected'].append(len(intersection))
        stats['perc True positive detected'].append(float(float(len(intersection)) / \
                                                          (1.0 + len(ref_snv_set))))
        stats['perc True positive detected ALL'].append(float(float(len(intersection)) \
                                                              / len(ref_snv_set_all)))
        stats['perc intersection detected PASS'].append(float(len(pass_snv)) / \
                                                        (1.0 + len(intersection)))
        stats['perc SNV covered by reads '].append(float(len(ref_snv_set)) / len(ref_snv_set_all))
        stats['nb. unique reads aligned'].append(self.nb_reads[gsm])
        snv_imputed_all = set()

        if self.snv_imputer is not None:
            snv_imputed_all = set(self.snv_imputer.missing_snvs[gsm].keys())
            # snv_imputed = set([snv for snv, count in
            #                    self.snv_imputer.missing_snvs[gsm].iteritems()
            #                    if count >= thres])

            all_snvs = snv_imputed_all.union(snv_set)

            self.total_imputed_snvs_container[thres].update(snv_imputed_all)

            stats['total nb snv imputed ALL'].append(len(snv_imputed_all))
            stats['all SNVs: original + imputed'].append(len(all_snvs))

            intersection_imputed_all = ref_snv_set.intersection(snv_imputed_all)

            stats['perc True positive detected from imputed ALL'].append(
                float(len(intersection_imputed_all)) / len(ref_snv_set))

            intersection_everything = ref_snv_set.intersection(snv_imputed_all.union(snv_set))

            stats['perc True positive detected from everything'].append(
                float(len(intersection_everything)) / len(ref_snv_set))
            stats['perc True positive detected from everything ALL'].append(
                float(len(intersection_everything)) / len(ref_snv_set_all))

        if snv_original:
            noise = set(snv_file.keys()) \
                    .difference(snv_original.keys()) \
                    .difference(ref_snv_set_all)

            noise_all = set(snv_file.keys()).union(snv_imputed_all) \
                                                .difference(snv_original.keys()) \
                                                .difference(ref_snv_set_all)

            noise = [snv for snv in noise if snv_file[snv]['depth'] >= thres]
            noise_all = [snv for snv in noise_all if snv_file[snv]['depth'] >= thres]

            noise = [snv for snv in noise if snv_file[snv]['depth'] >= thres]
            noise_all = [snv for snv in noise_all if snv_file[snv]['depth'] >= thres]

            stats['nb noise'].append(len(noise))
            stats['nb noise from everything'].append(len(noise_all))

    def _create_dicts(self, gsm):
        """
        """
        mutation_snv = '{0}/{1}/sim_mutation.bed'.format(OUTPUT_PATH_STAR, gsm)
        intersect_snv = '{0}/{1}/snv_intersect.bed'.format(OUTPUT_PATH_STAR, gsm)
        vcf_file = '{0}/data/{1}/snv_filtered.vcf'.format(self.output_path, gsm)
        vcf_original = '{0}/{1}/snv_filtered.vcf'.format(self.path_data_original, gsm)

        assert(isfile(mutation_snv))
        assert(isfile(intersect_snv))
        assert(isfile(vcf_file))

        if gsm not in self.nb_reads:
            self.nb_reads[gsm] = get_aligned_read_nb_from_star(gsm)

        if isfile(vcf_original) and gsm not in self.snv_originals:
            self.snv_originals[gsm] = self.create_snv_dict(vcf_original)

        if gsm not in self.ref_snv_set_alls:
            self.ref_snv_set_alls[gsm] = create_bed_snv_set(mutation_snv)

        if gsm not in self.snv_files:
            self.snv_files[gsm] = self.create_snv_dict(vcf_file)

        if gsm not in self.ref_snv_sets:
            self.ref_snv_sets[gsm] = create_bed_snv_set(intersect_snv, thres=0)


def create_bed_snv_set(fil, thres=1):
    """
    """
    bed_dict = {}

    for line in open(fil):
        line = line.strip('\n').split('\t')

        if line[-1].isdigit():
            count = int(line[-1])

            if thres and count < thres:
                continue
        else:
            count = 0

        bed_dict[(line[0], int(line[1]))] = count

    return bed_dict

def create_snv_dict_from_GATK(fil):
    """
    """
    snv_dict = {}

    for line in open(fil):
        if line[0] == '#':
            continue

        line = line.strip('\n').split('\t')
        snv_id = (line[0], int(line[1]))
        depth_total = sum(map(int, line[-1].split(':')[2].split(',')))

        snv_dict[snv_id] = {'status': line[6],
                            'depth': depth_total,
                            'len_variant': len(line[4])}

    return snv_dict

def create_snv_dict_from_freebayes(fil):
    """
    """
    snv_dict = {}

    for line in open(fil):
        if line[0] == '#':
            continue

        line = line.strip('\n').split('\t')
        snv_id = (line[0], int(line[1]))

        depth_total = sum(map(int, line[-1].split(':')[2].split(',')))
        snv_dict[snv_id] = {'status': line[6],
                            'depth': depth_total,
                            'len_variant': len(line[4]) }

    return snv_dict

def get_aligned_read_nb_from_star(gsm):
    """
    """
    star_log = '{0}/{1}/Log.final.out'.format(OUTPUT_PATH_STAR, gsm)

    return int(re.findall('Uniquely mapped reads number.+?(?P<readnb>[0-9]+)',
                      open(star_log).read())[0])

if __name__ == '__main__':
    main()
