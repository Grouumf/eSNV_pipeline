#! /usr/bin/python

""" process multiple SRR with eSNV"""

from multiprocessing import Process
from multiprocessing import Queue

from random import randint
from time import sleep
from os import listdir
from os.path import isfile
from os.path import isdir
from shutil import rmtree as rmdir

from SNV_pipeline.process_esnv import Process_eSNV
from SNV_pipeline.config import SPECIFIC_FILENAME_PATTERN
from SNV_pipeline.config import NB_PROCESS_ESNV as NB_PROCESS
from SNV_pipeline.config import PATH_OUTPUT
from SNV_pipeline.config import OUTPUT_PATH_ESNV


######## VARIABLE ############
CLEANING_MODE = False
##############################


def main():
    res = raw_input(
        "==> ready to launch eSNV on {0} processes\n continue? (Y/n)"\
              .format(NB_PROCESS))
    if res != 'Y':
        print('abord')
        return

    mp_analysis = Mp_Analysis()
    mp_analysis.run()

class Mp_Analysis():
    def __init__(self):
        """ """

        self.mp_queue = Queue()

        output_tophat = listdir(PATH_OUTPUT + "tophat/")
        output_mapsplice = listdir(PATH_OUTPUT + "mapsplice/")

        output_common = set(output_tophat).intersection(output_mapsplice)

        for fil in output_common:
            if not isfile(PATH_OUTPUT + "tophat/" + fil + "/accepted_hits.bam"):
                print('no tophat bam file for {0} skipping'.format(fil))

                if isdir(PATH_OUTPUT + "tophat/" + fil) and CLEANING_MODE:
                    rmdir(PATH_OUTPUT + "tophat/" + fil)
                continue

            if not isfile(PATH_OUTPUT + "mapsplice/" + fil + "/alignments.bam"):
                print('no mapsplice bam file for {0} skipping'.format(fil))

                if isdir(PATH_OUTPUT + "mapsplice/" + fil) and CLEANING_MODE:
                    rmdir(PATH_OUTPUT + "mapsplice/" + fil)
                continue

            if isdir("{0}/data/{1}".format(OUTPUT_PATH_ESNV, fil)):
                print('eSNV file output already exists for {0} skipping...'\
                    .format(fil))
                continue

            print("file to be processed:", fil)
            self.mp_queue.put(fil)

        print("\n #### now launching multiprocessing analysis #### \n")

        self.processes = [ESNV_multiprocessing(self.mp_queue)
                          for _ in range(NB_PROCESS)]
    def _run(self):
        for p in self.processes:
            p.start()

        while self.mp_queue.qsize():
            for p in self.processes:
                if p.exitcode:
                    raise KeyboardInterrupt
            sleep(1)

    def run(self):
        try:
            self._run()

        except KeyboardInterrupt:
            for p in self.processes:
                p.terminate()


class ESNV_multiprocessing(Process):
    """
    Launch and control several instance of eSNV process
    """
    def __init__(self, input_queue):
        self.input_queue = input_queue
        self.id = randint(0, 1000000)
        self.process_esnv = Process_eSNV(id=self.id)
        Process.__init__(self)

    def run(self):
        while self.input_queue.qsize():
            try:
                patient = self.input_queue.get(True, 0.2)
            except Exception as e:
                print("exception:{0}".format(e))
                continue
            else:
                print("processing for file {0} with id {1}"\
                    .format(patient, self.id))
                self.process_esnv.process(patient)


if __name__ == "__main__":
    main()
