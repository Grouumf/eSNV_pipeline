"""
config variable for GATK SNV pipeline\
valid sequencing machine for picard tools:
ILLUMINA, SLX, SOLEXA, SOLID, 454, LS454, COMPLETE, PACBIO,
IONTORRE NT, CAPILLARY, HELICOS, UNKNOWN

####  Dataset description: ####
(name, organism, sequencing machine, read length)
"""

from os.path import split as pathsplit


PROJECT_NAME = "10x_data_sim"

# either the number of snvs or None
SIMULATED_REF_GENOME = 50000

PROJECT_NAME_DICT = {
    "10x_data_sim": ("10x_data_sim", "HUMAN", 'ILLUMINA', 99),
    "10x_data_healthy_1_2_work": ("10x_data_healthy_1_2_work", "HUMAN", 'ILLUMINA', 99),
    "10x_data": ("10x_data", "MOUSE", 'ILLUMINA', 98),
    "10x_data_50_50": ("10x_data_50_50", "HUMAN", 'ILLUMINA', 99),
    "10x_data_original": ("10x_data_original", "MOUSE", 'ILLUMINA', 98),
    "hou": ("hou_sctrio_2016", "HUMAN", 'ILLUMINA', 101),
    "kumar": ("mrna_yale", "HUMAN", 'ILLUMINA', 55),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE75688
    "chung": ("chung_notch_2016", "HUMAN", 'ILLUMINA', 100),
    "pancreas": ("pancreatic_circulating_tumor", 'MOUSE', 'ILLUMINA', 50),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE51372
    "waterfall": ("waterfall_neural_stemcells", 'MOUSE', 'ILLUMINA', 100),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE71485
    "kim": ("kim_renal_carcinoma", 'HUMAN', 'ILLUMINA', 100),
    "kim_reduced": ("kim_reduced", 'HUMAN', 'ILLUMINA', 100),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE73121xb
    "kim_reduced10K": ("kim_reduced10K", 'HUMAN', 'ILLUMINA', 100),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE73121xb
    "glio": ("glioblastoma", 'HUMAN', 'ILLUMINA', 25),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE57872
    "glio_L": ("glioblastoma_L", 'HUMAN', 'ILLUMINA', 100),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE57872
    "lung": ("lung_epithelium", 'MOUSE', 'ILLUMINA', 101),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE52583
    "CTC": ("2015_human_CTC_prostate", 'HUMAN', 'ILLUMINA', 50),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE67980
    "weterung": ("weterung_2016_organoid", 'HUMAN', 'ILLUMINA', 75),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE65253
    "braune": ("braune_2016_breast_cancer", 'HUMAN', "ILLUMINA", 43),
    # http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE77308
}

try:
    (PROJECT_NAME,
     CELL_TYPE,
     PLATEFORM,
     STAR_INDEX_READ_LENGTH) = PROJECT_NAME_DICT[PROJECT_NAME]

except Exception as e:
    from warnings import warn
    warn('exception found in config.py!: {0}'.format(e))

    PROJECT_NAME = None
    CELL_TYPE = None
    PLATEFORM = None
    STAR_INDEX_READ_LENGTH = None

############ FOLDER ARCHITECTURE RNA2 ################################
GLOBAL_DATA_ROOT = '/data/opoirion/'
PROJECT_ROOT = '/home/opoirion/'
OUTPUT_ROOT = '/home/opoirion/'
PROG_ROOT = '/home/opoirion/prog/'
CODE_ROOT = '/home/opoirion/code/'
######################################################################

############ FOLDER ARCHITECTURE SERVER ##############################
# GLOBAL_DATA_ROOT = '/home/opoirion/lus/refdata/'
# PROJECT_ROOT = '/home/opoirion/lus/refdata/'
# OUTPUT_ROOT = '/home/opoirion/lus/'
# PROG_ROOT = '/home/opoirion/apps/prog/'
# CODE_ROOT = '/home/opoirion/apps/code/'
######################################################################

############ STANDART VARIABLE #######################################
TYPE_VAR = {
    'HUMAN': {
        'BOWTIE': "{0}/Illumina_hg19/BowtieIndex/"\
        .format(GLOBAL_DATA_ROOT),
        'BOWTIE2': "{0}/Illumina_hg19//BowtieIndex2/genome"\
        .format(GLOBAL_DATA_ROOT),
        'ANNOTATION_PATH': "{0}/Illumina_hg19/Annotation/genes.gtf"\
        .format(GLOBAL_DATA_ROOT),
        'GENOME_PATH': "{0}/Illumina_hg19/Chromosomes"\
        .format(GLOBAL_DATA_ROOT),
        'STAR_INDEX_PATH': "{0}/Illumina_hg19/Sequences/STARindex"\
        .format(GLOBAL_DATA_ROOT),
        'REF_GENOME': "{0}/Illumina_hg19/Sequences/WholeGenomeFasta/genome.fa"\
        .format(GLOBAL_DATA_ROOT),
        'PATH_CHR': "{0}/Illumina_hg19/Chromosomes/"\
        .format(GLOBAL_DATA_ROOT),
        'ORGANISM': 'hg19',
        'DBSNP': "{0}/Illumina_hg19/vcf/dbsnp_138.hg19.vcf"\
        .format(GLOBAL_DATA_ROOT),
        'VCF_RESOURCES': [
            "{0}/Illumina_hg19/vcf/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf"\
            .format(GLOBAL_DATA_ROOT),
            "{0}/Illumina_hg19/vcf/1000G_phase1.indels.hg19.sites.vcf"\
            .format(GLOBAL_DATA_ROOT),
            ]
    },
    'MOUSE': {
        'BOWTIE': "{0}/Mus_musculus/UCSC/mm10/Sequence/BowtieIndex/"\
        .format(GLOBAL_DATA_ROOT),
        'BOWTIE2': "{0}/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/mm10"\
        .format(GLOBAL_DATA_ROOT),
        'ANNOTATION_PATH': "{0}/Mus_musculus/UCSC/mm10/Annotation/genes.gtf"\
        .format(GLOBAL_DATA_ROOT),
        'GENOME_PATH': "{0}/Mus_musculus/UCSC/mm10/Sequence/Chromosomes"\
        .format(GLOBAL_DATA_ROOT),
        'STAR_INDEX_PATH': "{0}/Mus_musculus/UCSC/mm10/Sequence/STARindex"\
        .format(GLOBAL_DATA_ROOT),
        'PATH_CHR': "{0}/Mus_musculus/UCSC/mm10/Sequence/Chromosomes/"\
        .format(GLOBAL_DATA_ROOT),
        'REF_GENOME': "{0}/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa"\
        .format(GLOBAL_DATA_ROOT),
        'ORGANISM': 'mm10',
        'DBSNP': "{0}/Mus_musculus/vcf/mgp.v3.snps.rsIDdbSNPv137_ordered.vcf"\
        .format(GLOBAL_DATA_ROOT),
        'VCF_RESOURCES': [
            "{0}/Mus_musculus/vcf/mgp.v3.indels.rsIDdbSNPv137_ordered.vcf"\
            .format(GLOBAL_DATA_ROOT)
            ]
    }
}
#####################################################################

############ MOUSE/ HUMAN ###########################################
BOWTIE = TYPE_VAR[CELL_TYPE]['BOWTIE']
GENOME_PATH = TYPE_VAR[CELL_TYPE]['GENOME_PATH']
BOWTIE2 = TYPE_VAR[CELL_TYPE]['BOWTIE2']
REF_GENOME = TYPE_VAR[CELL_TYPE]['REF_GENOME']
ANNOTATION_PATH = TYPE_VAR[CELL_TYPE]['ANNOTATION_PATH']
STAR_INDEX_PATH = TYPE_VAR[CELL_TYPE]['STAR_INDEX_PATH']
ORGANISM = TYPE_VAR[CELL_TYPE]['ORGANISM']
DBSNP = TYPE_VAR[CELL_TYPE]['DBSNP']
VCF_RESOURCES = TYPE_VAR[CELL_TYPE]['VCF_RESOURCES']
PATH_CHR = TYPE_VAR[CELL_TYPE]['PATH_CHR']
#####################################################################

############# DATASET ###############################################
FASTQ_PATH = "{1}/{0}/fastq/".format(PROJECT_NAME, GLOBAL_DATA_ROOT)
PATH_OUTPUT = "{0}/data/{1}/".format(OUTPUT_ROOT, PROJECT_NAME)
PATH_OUTPUT_INDEX = "{0}/data/{1}/gtf_index_{2}/"\
                    .format(OUTPUT_ROOT, PROJECT_NAME, CELL_TYPE)
SPECIFIC_FILENAME_PATTERN = ""
#####################################################################

############# SOFTWARE ##############################################
JAVA = "{0}/jdk1.8.0_77/bin/java".format(PROG_ROOT)
JAVA_MEM = "-Xmx110g"
GATK_DIR = "{0}/GATK/".format(PROG_ROOT)
PICARD_DIR = "{0}/picard-tools-2.1.1".format(PROG_ROOT)
PATH_HTML = "{0}/d3visualisation/".format(CODE_ROOT) # OPTIONAL
FASTQC = "fastqc"
SNPEFF = '{0}/snpEff/snpEff.jar'.format(PROG_ROOT)
SNPEFF_DICT = {'MOUSE': 'GRCm38.82',
               'HUMAN': 'GRCh37.75'}
SNPEFF_DB = SNPEFF_DICT[CELL_TYPE]
SAMTOOLS = '{0}/samtools-1.5/bin/samtools'.format(PROG_ROOT)
CUFFLINKS = 'cufflinks'
#####################################################################

#############  STAR #################################################
PATH_STAR_SOFTWARE = "{0}/STAR/bin/Linux_x86_64_static/STAR"\
                          .format(PROG_ROOT)
STAR_THREADS = 12
OUTPUT_PATH_STAR = PATH_OUTPUT + "/star/"
#####################################################################

############# MONOVAR ###############################################
PATH_MONOVAR = '{0}/monovar/'.format(PATH_OUTPUT)
PATH_MONOVAR_INPUT = '{0}/monovar_input_all.txt'.format(PATH_MONOVAR)
PATH_MONOVAR_REGION = '{0}/monovar_input_region.txt'.format(PATH_MONOVAR)
CHUNCK_SIZE = None
CHR_CHUNK_SIZE = 10000000
MONOVAR_THREAD_NB = 3
MONOVAR_REP = '{0}/monovar/'.format(PROG_ROOT)
NB_PROCESS_MONOVAR = 5
PYTHON = 'python2.7'
#####################################################################

#############  MAPSPLICE ############################################
OUTPUT_PATH_MAPSPLICE = PATH_OUTPUT + "/mapsplice/"
PATH_MAPSPLICE_SOFTWARE = "{0}/MapSplice-v2.1.8/mapsplice.py"\
                          .format(PROG_ROOT)
MAPSPLICE_THREADS = 12
#####################################################################

################ TOPHAT #############################################
OUTPUT_PATH_TOPHAT = PATH_OUTPUT + "/tophat/"
TOPHAT_THREADS = 12
TOPHAT_OPTION = ""
PATH_TOPHAT_SOFTWARE = "tophat"
#####################################################################

############ PROCESS eSNV ###########################################
OUTPUT_PATH_ESNV =  PATH_OUTPUT + '/esnv_raw_result/'
ESNV_PATH = "{0}/eSNV-Detect_1.0/".format(PROG_ROOT)
ESNV_CONFIG_PATH = CODE_ROOT + \
 '/SNV_pipeline/SNV_pipeline/eSNV_{0}_config.txt'.format(CELL_TYPE)
NB_PROCESS_ESNV = 1
####################################################################

############ GATK SNV CALLING PIPELINE ##########################
OUTPUT_PATH_GATK =  PATH_OUTPUT + '/snv_pipeline_raw/'
PATH_TO_DATA = PATH_OUTPUT
NB_PROCESS_GATK = 5
####################################################################

########### FREEBAYES SNV CALLING PIPELINE ##########################
OUTPUT_PATH_FREEBAYES = PATH_OUTPUT + '/freebayes/'
PATH_OPOSSUM = '{0}/Opossum/'.format(PROG_ROOT)
PATH_FREEBAYES = '{0}/freebayes/bin/freebayes'.format(PROG_ROOT)
####################################################################

############ COMPUTE DISTANCE MATRIX ##########################################
FEATURE_COUNT = "{0}/subread-1.5.0-p1-Linux-x86_64/bin/featureCounts"\
                      .format(PROG_ROOT)
MATRIX_OUTPUT_PATH = "{0}/data/{1}/expression_profile/"\
                     .format(OUTPUT_ROOT, PROJECT_NAME)
###############################################################################


######################## SNV SIMULATION #######################################
if SIMULATED_REF_GENOME:
    MUTATION_FILE = '{0}/Simulated{1}Mut/sim_snv.bed'.format(
        pathsplit(pathsplit(REF_GENOME)[0])[0], SIMULATED_REF_GENOME)
    SEQUENCES_PATH = pathsplit(pathsplit(REF_GENOME)[0])[0]
    SIM_GENOME_DIR = '{0}/Simulated{1}Mut/'.format(SEQUENCES_PATH, SIMULATED_REF_GENOME)
    REF_GENOME_ORIGINAL = REF_GENOME[:]
    REF_GENOME = '{0}/genome.fa'.format(SIM_GENOME_DIR)
else:
    MUTATION_FILE = None
    SEQUENCES_PATH = None
    SIM_GENOME_DIR = None
    REF_GENOME_ORIGINAL = None
###############################################################################
