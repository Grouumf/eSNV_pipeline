from SNV_pipeline.config import OUTPUT_PATH_STAR

from glob import glob

from pysam import VariantFile

from os.path import isfile
from os.path import split as pathsplit

from collections import defaultdict
from collections import deque

from sklearn.decomposition import TruncatedSVD

from sklearn.neighbors import NearestNeighbors

from scipy.sparse import csr_matrix
from scipy.sparse import lil_matrix

import numpy as np

from sys import stdout


def main():
    """
    """
    # from SNV_pipeline.config import OUTPUT_PATH_GATK
    from SNV_pipeline.config import OUTPUT_PATH_FREEBAYES

    snv_imputer = SNVImputer(OUTPUT_PATH_FREEBAYES)
    snv_imputer.fit()

class SNVImputer():
    """
    """
    def __init__(self,
                 path_snv,
                 output_path_star=OUTPUT_PATH_STAR,
                 nb_iter=4,
                 thres=1):
        self.path_snv = path_snv
        self.output_path_star = output_path_star

        self.nb_iter = nb_iter

        self.cell_dicts = {}
        self.depth_dicts = {}

        self.snv_index = {}
        self.cell_index = {}
        self.snv_index_rev = {}
        self.cell_index_rev = {}

        self.missing_values = []
        self.imputed_values = None
        self.missing_snvs = defaultdict(dict)

        self.data_frame = None
        self.surprise_data = None
        self.algo = None

        self.sparse_matrix = None
        self.reconstructed_matrix = None
        self.latent_space = None
        self.thres = thres

    def create_matrix(self):
        """
        """
        self.missing_values = []

        scores = []
        rows = []
        cols = []

        nb_cells = len(self.cell_dicts)
        count = 0

        for cell in self.cell_dicts:
            for snv in self.snv_index:

                is_snv = 1.0 if snv in self.cell_dicts[cell] else -1.0
                score = is_snv * self.depth_dicts[cell][snv]
                index_cell = self.cell_index[cell]
                index_snv = self.snv_index[snv]

                if not score:
                    self.missing_values.append((index_cell, index_snv))
                else:
                    scores.append(score)
                    rows.append(index_cell)
                    cols.append(index_snv)

            count += 1
            stdout.write('\r {0} / {1} samples done...'.format(count, nb_cells))
            stdout.flush()

        self.sparse_matrix = csr_matrix(
            (scores, (rows, cols)),
            shape=(len(self.cell_index),len(self.snv_index)))

        self.missing_values = np.array(self.missing_values)

    def fit_procedure_using_svd(self):
        """
        """
        self.algo = TruncatedSVD(n_components=5, n_iter=50)

        for i in range(self.nb_iter):
            self._fit_svd()
            self._find_imputed_values_and_construct_dicts()
            self._update_sparse_matrix()

    def fit_procedure_using_knn(self):
        """
        """
        self.algo = NearestNeighbors(n_neighbors=3, metric='cityblock')
        self._fit_with_knn()

    def _fit_with_knn(self):
        """
        """
        self.algo.fit(self.sparse_matrix)

        distances, neighbors = self.algo.kneighbors(self.sparse_matrix)

        self.reconstructed_matrix = self.sparse_matrix.todok()
        count = 0
        cell_count = 0

        cell_tot = self.reconstructed_matrix.shape[0]

        raws, cols, values = deque(), deque(), deque()

        for cell_i, neighbor in enumerate(neighbors):
            vectors = self.sparse_matrix[neighbor]
            cells, snvs = vectors.nonzero()
            stdout.write('#\r cell {0} / {1}'.format(cell_count , cell_tot))
            stdout.flush()

            cell_count += 1

            for snv_i in snvs:
                value = vectors.T[snv_i].mean()
                if self.reconstructed_matrix[cell_i, snv_i] <= 0:
                    raws.append(cell_i)
                    cols.append(snv_i)
                    values.append(value)
                    # self.reconstructed_matrix[cell_i, snv_i] += value

                    if self.reconstructed_matrix[cell_i, snv_i] + value > 0:
                        cell = self.cell_index_rev[cell_i]
                        snv = self.snv_index_rev[snv_i]
                        self.missing_snvs[cell][snv] = value
                        count += 1

        print('#### Missing values imputed {0}'.format(count))

    def fit(self):
        """
        """
        print('#### fitting missing snvs...')
        print('\n ## create dictionary...')
        self.create_dictionnary()
        print('\n ## create matrix...')
        self.create_matrix()
        print('\n ## fitting truncated SVD...')
        self.fit_procedure_using_knn()

    def _find_imputed_values_and_construct_dicts(self):
        """
        """
        imputed_values = self.reconstructed_matrix[
            self.missing_values.T[0], self.missing_values.T[1]]

        self.imputed_coordinate = self.missing_values[imputed_values > self.thres]
        self.imputed_values = imputed_values[imputed_values > self.thres]

        print('#### {0} missing values imputed'.format(len(self.imputed_values)))

        for (cell_i, snv_i), value in zip(self.imputed_coordinate, self.imputed_values):
            cell = self.cell_index_rev[cell_i]
            snv = self.snv_index_rev[snv_i]
            self.missing_snvs[cell][snv] = value

    def _update_sparse_matrix(self):
        """
        """
        self.sparse_matrix = lil_matrix(self.sparse_matrix)

        self.sparse_matrix[
            self.imputed_coordinate.T[0],
            self.imputed_coordinate.T[1]
        ] = self.imputed_values

        self.sparse_matrix = csr_matrix(self.sparse_matrix)

    def _fit_svd(self):
        """
        """
        self.latent_space = self.algo.fit_transform(
            self.sparse_matrix)
        self.reconstructed_matrix = self.algo.inverse_transform(
            self.latent_space)

    def create_dictionnary(self):
        """
        """
        cell_set = set()
        snv_set = set()

        file_list = glob('{0}/data/*'.format(self.path_snv))
        nb_files = len(file_list)

        count = 0

        for gsm in file_list:
            vcf_file = '{0}/snv_filtered.vcf'.format(gsm)
            cell = pathsplit(gsm)[1]

            intersect_snv = '{0}/{1}/snv_intersect.bed'.format(OUTPUT_PATH_STAR, cell)

            if not isfile(vcf_file):
                continue

            cell_dict = create_vcf_dict(vcf_file)
            depth_dict = create_read_depth_dict(intersect_snv)

            self.cell_dicts[cell] = cell_dict
            self.depth_dicts[cell] = depth_dict

            snv_set.update(cell_dict.iterkeys())
            cell_set.add(cell)

            count += 1

            stdout.write('\r {0} / {1} samples done...'.format(count, nb_files))
            stdout.flush()

        for pos, cell in enumerate(cell_set):
            self.cell_index[cell] = pos
            self.cell_index_rev[pos] = cell

        for pos, snv in enumerate(snv_set):
            self.snv_index[snv] = pos
            self.snv_index_rev[pos] = snv

def create_read_depth_dict(fil):
    """
    """
    depth_dict = defaultdict(int)

    for line in open(fil):
        line = line.strip('\n').split('\t')
        depth = int(line[-1])

        if depth:
            depth_dict[(line[0], int(line[1]))] = depth

    return depth_dict

def create_vcf_dict(fil):
    """
    """
    vcf = VariantFile(fil)
    vcf_dict = defaultdict(float)

    while True:
        try:
            variant = vcf.next()

        except Exception:
            break

        if 'PASS' not in variant.filter:
            continue

        vcf_dict[(variant.chrom, variant.pos)] = variant.info['DP']

    return vcf_dict


if __name__ == '__main__':
    main()
