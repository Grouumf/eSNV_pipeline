from SNV_pipeline.config import ANNOTATION_PATH
from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import REF_GENOME_ORIGINAL
from SNV_pipeline.config import PATH_CHR
from SNV_pipeline.config import PATH_OUTPUT
from SNV_pipeline.config import PICARD_DIR
from SNV_pipeline.config import STAR_INDEX_PATH
from SNV_pipeline.config import STAR_INDEX_READ_LENGTH
from SNV_pipeline.config import SAMTOOLS
from SNV_pipeline.config import SIMULATED_REF_GENOME
from SNV_pipeline.config import SIM_GENOME_DIR

from collections import defaultdict

from subprocess import check_output

from os.path import isfile
from os import mkdir
from os.path import isdir
from os.path import split as pathsplit

from os import remove
from os import popen

from shutil import copy

from glob import glob

import numpy as np


######################## VARIABLE ############################
ADDSNVS = '/home/opoirion/prog/bamsurgeon/bin/addsnv.py'
PATH_TMP_BED = PATH_OUTPUT + '/tmp/random_mutations.bed'
PICARD_JAR = PICARD_DIR + '/picard.jar'
STAR_INDEX = '{0}READ{1}'.format(STAR_INDEX_PATH, STAR_INDEX_READ_LENGTH)
##############################################################


def main():
    """
    """
    random_mutation = RandomMutation()
    random_mutation.select_exons(SIMULATED_REF_GENOME)
    random_mutation.select_mutations()
    random_mutation.generate_mutated_ref_genome()
    random_mutation.create_bed_file()

    random_mutation.complete_missing_chromosomes()
    random_mutation.aggregate_and_create_index()


class RandomMutation(object):
    """
    """
    def __init__(self):
        """
        """
        if not isdir(SIM_GENOME_DIR):
            mkdir(SIM_GENOME_DIR)

        self.nb_exon = 0
        self.exon_list = []
        self.weight_list = []
        self.weight_list_sampled = []
        self.total_length = None
        self.exon_sampled = []
        self.order = []
        self.mutations_selected = defaultdict(list)
        self.mutations_introduced = []

        self.new_dir = None

        self._process_gtf_file()

    def generate_mutated_ref_genome(self):
        """
        """
        bases = {'A', 'T', 'G', 'C'}

        self.mutations_introduced = []

        for chro in self.mutations_selected:
            chr_ref = open(PATH_CHR + '/{0}.fa'.format(chro))
            chr_ref.readline()

            chr_sim = open(SIM_GENOME_DIR + '/{0}.fa'.format(chro), 'w')
            chr_sim.write('>{0}\n'.format(chro))

            chr_list = []

            for line in chr_ref:
                chr_list += list(line.strip('\n'))

            for mutation in self.mutations_selected[chro]:
                mutation = mutation - 1
                old_base = chr_list[mutation].upper()
                new_base = np.random.choice(
                    list(bases.difference(old_base)))
                chr_list[mutation] = new_base

                self.mutations_introduced.append((
                    chro, mutation + 1, old_base, new_base))

            i = 0
            nb_bases = len(chr_list)

            while i < nb_bases:
                chr_sim.write(''.join(chr_list[i:i + 50]) + '\n')
                i += 50

            print('{0} generated'.format(chro))

    def complete_missing_chromosomes(self):
        """
        """
        for fil in glob('{0}/*.fa'.format(PATH_CHR)):
            chro = pathsplit(fil)[1].split('.')[0]

            if chro not in self.mutations_selected:
                copy(fil, '{0}/{1}.fa'.format(SIM_GENOME_DIR, chro))
                print('{0} copied from the original folder'.format(chro))

    def aggregate_and_create_index(self):
        """
        """
        self.order = []
        f_genome_dict = open('{0}.dict'.format(REF_GENOME_ORIGINAL.rsplit('.')[0]))
        f_genome_dict.readline()

        for line in f_genome_dict:
            line = line.split('\t')
            self.order.append(line[1].split(':')[-1])

        if isfile('{0}/genome.fa'.format(SIM_GENOME_DIR)):
            remove('{0}/genome.fa'.format(SIM_GENOME_DIR))

        for chro in self.order:
            cmd = 'cat {0}/{1}.fa >> {0}/genome.fa'.format(SIM_GENOME_DIR, chro)
            popen(cmd).read()

        cmd = 'rm {0}/genome.dict'.format(SIM_GENOME_DIR)
        popen(cmd).read()
        cmd = 'rm {0}/genome.fa.fai'.format(SIM_GENOME_DIR)
        popen(cmd).read()

        cmd = 'java -jar {0}/picard.jar CreateSequenceDictionary'\
              ' R= {1}/genome.fa O= {1}/genome.dict'.format(PICARD_DIR, SIM_GENOME_DIR)
        check_output(cmd.split())

        cmd = '{0} faidx {1}/genome.fa'.format(SAMTOOLS, SIM_GENOME_DIR)
        check_output(cmd.split())

        cmd = 'rm {0}/chr*.fa'.format(SIM_GENOME_DIR)
        popen(cmd).read()

    def _process_gtf_file(self):
        """
        """
        f_gtf = open(ANNOTATION_PATH)

        for line in f_gtf:
            line = line.split()

            if line[2] != 'exon':
                continue

            if len(line[0][3:]) > 2:
                continue

            self.exon_list.append((line[0], int(line[3]), int(line[4])))
            self.weight_list.append(float(line[4]) - int(line[3]))

        self.weight_list = np.array(self.weight_list)
        self.total_length = self.weight_list.sum()

        self.weight_list /= self.total_length

    def select_exons(self, n_exons):
        """
        """
        exon_sampled_id = np.random.choice(range(len(self.exon_list)),
                                           n_exons,
                                           p=self.weight_list)
        self.exon_sampled = [self.exon_list[id] for id in exon_sampled_id]
        return self.exon_sampled

    def select_mutations(self):
        """
        """
        self.mutations_selected = defaultdict(list)

        for chro, start , stop in self.exon_sampled:
            pos = np.random.choice(stop-start, 1)[0]

            self.mutations_selected[chro].append(start + pos)

    def create_bed_file(self):
        """
        """
        f_bed = open('{0}/sim_snv.bed'.format(SIM_GENOME_DIR), 'w')

        for chro, pos, old_base, new_base in self.mutations_introduced:

                f_bed.write('{0}\t{1}\t{1}\t{2}\t{3}\n'.format(
                    chro, pos, old_base, new_base))

        print('{0}/sim_snv.bed created'.format(SIM_GENOME_DIR))


if __name__ == '__main__':
    main()
