from glob import glob

from os import popen

import re

from multiprocessing import Pool
from sys import argv


if "--file_id" in argv:
    FILE_ID = argv[
        argv.index("--file_id") + 1]
else:
    FILE_ID = None

if "--path_monovar" in argv:
    PATH_MONOVAR = argv[
        argv.index("--path_monovar") + 1]
else:
    from config import PATH_MONOVAR

if "--monovar_rep" in argv:
    MONOVAR_REP = argv[
        argv.index("--monovar_rep") + 1]
else:
    from config import MONOVAR_REP

if "--samtools" in argv:
    SAMTOOLS = argv[
        argv.index("--samtools") + 1]
else:
    from config import SAMTOOLS

if "--python" in argv:
    PYTHON = argv[
        argv.index("--python") + 1]
else:
    from config import PYTHON

if "--nb_process_monovar" in argv:
    NB_PROCESS_MONOVAR = int(argv[
        argv.index("--nb_process_monovar") + 1])
else:
    from config import NB_PROCESS_MONOVAR

if "--ref_genome" in argv:
    REF_GENOME = argv[
        argv.index("--ref_genome") + 1]
else:
    from config import REF_GENOME

if "--monovar_thread_nb" in argv:
    MONOVAR_THREAD_NB = int(argv[
        argv.index("--monovar_thread_nb") + 1])
else:
    from config import MONOVAR_THREAD_NB

if "--region" in argv:
    REGION = argv[
        argv.index("--region") + 1]
else:
    REGION = None


def main():
    """ """
    if FILE_ID:
        launch_monovar_for_one_file(FILE_ID, REGION)
    else:
        launch_monovar_for_all_the_files()

def launch_monovar_for_one_file(file_id, region=None):
    """
    """
    if region:
        region_r = '-r {0}'.format(region)
        region_o = '_{0}'.format(region)
    else:
        region_r = ''
        region_o = ''

    cmd = '{0} mpileup -BQ0 -f {1} -b {6} {8}'\
          '| {3} {4}/src/monovar.py -p 0.002 -a 0.2 -t 0.05 -f {1}'\
          ' -b {6} -m {5} -o {7}{9}.vcf'\
          .format(SAMTOOLS,
                  REF_GENOME,
                  PATH_MONOVAR,
                  PYTHON,
                  MONOVAR_REP,
                  NB_PROCESS_MONOVAR,
                  file_id, file_id.rsplit('.', 1)[0],
                  region_r,
                  region_o)

    _multiprocessing_func(cmd)

def launch_monovar_for_all_the_files():
    """
    """
    cmd_list = []

    for fil in glob('{0}/monovar_input_nb*.txt'.format(PATH_MONOVAR)):

        cmd = '{0} mpileup -BQ0  -f {1} -b {6} --region chr1:1-100000'\
              '| {3} {4}/src/monovar.py -p 0.002 -a 0.2 -t 0.05 -f {1}'\
              ' -b {6} -m {5} -o {7}.vcf'\
              .format(SAMTOOLS,
                      REF_GENOME,
                      PATH_MONOVAR,
                      PYTHON,
                      MONOVAR_REP,
                      NB_PROCESS_MONOVAR,
                      fil, fil.rsplit('.', 1)[0])

        cmd_list.append(cmd)

    pool = Pool(MONOVAR_THREAD_NB)
    pool.map(_multiprocessing_func, cmd_list)

def _multiprocessing_func(cmd):
    """ """
    print('###### command launched:\n{0}\n########'.format(cmd))
    fil  = re.findall('-f (?P<file>.+?) -b', cmd)[0]

    popen(cmd).read()

    print('monovar finished for: {0}').format(fil)


if __name__ == "__main__":
    main()
