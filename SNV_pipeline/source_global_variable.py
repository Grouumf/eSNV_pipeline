"""prepare global variables for bash sbatch scripts """

from os import getenv

from SNV_pipeline.config import PROJECT_NAME
from SNV_pipeline.config import PROJECT_ROOT
from SNV_pipeline.config import CODE_ROOT
from SNV_pipeline.config import OUTPUT_ROOT
from SNV_pipeline.config import GENOME_PATH
from SNV_pipeline.config import FASTQ_PATH
from SNV_pipeline.config import STAR_INDEX_PATH
from SNV_pipeline.config import STAR_INDEX_READ_LENGTH
from SNV_pipeline.config import PLATEFORM
from SNV_pipeline.config import ORGANISM
from SNV_pipeline.config import PATH_TO_DATA
from SNV_pipeline.config import DBSNP
from SNV_pipeline.config import VCF_RESOURCES
from SNV_pipeline.config import OUTPUT_PATH_STAR
from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import MONOVAR_THREAD_NB
from SNV_pipeline.config import NB_PROCESS_MONOVAR
from SNV_pipeline.config import PYTHON
from SNV_pipeline.config import SAMTOOLS
from SNV_pipeline.config import PATH_MONOVAR
from SNV_pipeline.config import MONOVAR_REP
from SNV_pipeline.config import PATH_MONOVAR_INPUT
from SNV_pipeline.config import PATH_MONOVAR_REGION
from SNV_pipeline.config import SIMULATED_REF_GENOME


def main():
    pathfile = '{0}/.bash_variable_{1}.sh'.format(getenv("HOME"),
                                                  PROJECT_NAME)

    with open(pathfile, 'w') as f:
        f.write('PROJECT_NAME={0}\n'.format(PROJECT_NAME))
        f.write('PROJECT_ROOT={0}\n'.format(PROJECT_ROOT))
        f.write('CODE_ROOT={0}\n'.format(CODE_ROOT))
        f.write('OUTPUT_ROOT={0}\n'.format(OUTPUT_ROOT))
        f.write('GENOME_PATH={0}\n'.format(GENOME_PATH))
        f.write('FASTQ_PATH={0}\n'.format(FASTQ_PATH))
        f.write('STAR_INDEX_PATH={0}\n'.format(STAR_INDEX_PATH))
        f.write('STAR_INDEX_READ_LENGTH={0}\n'.format(STAR_INDEX_READ_LENGTH))
        f.write('PLATEFORM={0}\n'.format(PLATEFORM))
        f.write('ORGANISM={0}\n'.format(ORGANISM))
        f.write('PATH_TO_DATA={0}\n'.format(PATH_TO_DATA))
        f.write('DBSNP={0}\n'.format(DBSNP))
        f.write('REF_GENOME={0}\n'.format(REF_GENOME))
        f.write('VCF_RESOURCES="{0}"\n'.format(str(VCF_RESOURCES).replace(' ', '')))
        f.write('OUTPUT_PATH_STAR={0}\n'.format(OUTPUT_PATH_STAR))
        f.write('MONOVAR_THREAD_NB={0}\n'.format(MONOVAR_THREAD_NB))
        f.write('NB_PROCESS_MONOVAR={0}\n'.format(NB_PROCESS_MONOVAR))
        f.write('PYTHON={0}\n'.format(PYTHON))
        f.write('SAMTOOLS={0}\n'.format(SAMTOOLS))
        f.write('PATH_MONOVAR={0}\n'.format(PATH_MONOVAR))
        f.write('MONOVAR_REP={0}\n'.format(MONOVAR_REP))
        f.write('PATH_MONOVAR_INPUT={0}\n'.format(PATH_MONOVAR_INPUT))
        f.write('PATH_MONOVAR_REGION={0}\n'.format(PATH_MONOVAR_REGION))
        f.write('SIMULATED_REF_GENOME={0}\n'.format(SIMULATED_REF_GENOME))


if __name__ == "__main__":
    main()
