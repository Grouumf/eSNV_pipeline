from SNV_pipeline.config import OUTPUT_PATH_STAR
from SNV_pipeline.config import PATH_OUTPUT

from subprocess import check_output

from multiprocessing import Pool

from glob import glob

from os.path import isdir
from os.path import isfile
from os.path import split as pathsplit

from os import mkdir

from os import popen


################ VARIABLE ########################
PATH_BED = '{0}/bed_files/'.format(PATH_OUTPUT)
NB_THREADS = 10
##################################################

check_output('bedtools --version'.split())

if not isdir('{0}/bed_files/'.format(PATH_OUTPUT)):
    mkdir('{0}/bed_files/'.format(PATH_OUTPUT))


def main():
    """ """
    cmd_list = []

    for folder in glob('{0}/*'.format(OUTPUT_PATH_STAR)):
        if not isdir(folder) and not isfile('{0}/Aligned.sortedByCoord.out.bam'.format(folder)):
            continue

        bam_file = '{0}/Aligned.sortedByCoord.out.bam'.format(folder)
        gsm = pathsplit(folder)[1]

        cmd_1 = 'bedtools bamtobed -i {0} > {1}/{2}.bed'.format(bam_file, PATH_BED, gsm)
        cmd_2 = 'gzip {0}/{1}.bed'.format(PATH_BED, gsm)

        cmd_list.append((cmd_1, cmd_2))

    pool = Pool(NB_THREADS)
    pool.map(_process_bed, cmd_list)

def _process_bed(cmds):
    """ """
    print('gor for: {0}'.format(cmds[0]))

    for cmd in cmds:
        popen(cmd).read()


if __name__ == '__main__':
    main()
