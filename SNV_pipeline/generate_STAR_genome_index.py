"""generate STAR GENOME INDEX"""


from SNV_pipeline.bash_utils import exec_cmd

from os.path import isdir
from os.path import isfile
from os.path import split as pathsplit
from distutils.dir_util import mkpath

from SNV_pipeline.config import PATH_STAR_SOFTWARE
from SNV_pipeline.config import STAR_INDEX_PATH
from SNV_pipeline.config import ANNOTATION_PATH
from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import STAR_THREADS
from SNV_pipeline.config import STAR_INDEX_READ_LENGTH
from SNV_pipeline.config import SIMULATED_REF_GENOME

from SNV_pipeline.server.single_cell_client import printf


def generate_star_index(
        star_index_path=STAR_INDEX_PATH,
        star_threads=STAR_THREADS,
        ref_genome=REF_GENOME,
        annotation_path=ANNOTATION_PATH,
        star_index_read_length=STAR_INDEX_READ_LENGTH,
        path_star_software=PATH_STAR_SOFTWARE,
        simulated_ref_genome=SIMULATED_REF_GENOME,
        stdout=None,
        tmp_folder=None,
        printf=printf):
    """ """

    star_index_path_updated = "{0}READ{1}/".format(star_index_path.rstrip('/'),
                                                   star_index_read_length)

    if simulated_ref_genome:
        star_index_path_updated = star_index_path_updated.rstrip('/') \
                          + 'SIM{0}'.format(simulated_ref_genome)

        ref_genome = '{0}/Simulated{1}Mut/genome.fa'.format(
            pathsplit(pathsplit(ref_genome)[0])[0], simulated_ref_genome)
        assert(isfile(ref_genome))

    printf("######## computing STAR index ########\npath:{0}\n"
        .format(star_index_path_updated))

    if not isdir(star_index_path_updated):
        mkpath(star_index_path_updated)

    if tmp_folder is None:
        tmp_folder = star_index_path_updated

    tmp_path = '{0}/_STARindextmp'.format(tmp_folder)

    if isdir(tmp_path):
        exec_cmd('rm -r {0}'.format(tmp_path), stdout)

    cmd = "{0} --runMode genomeGenerate --runThreadN {1}"\
          " --genomeDir {2} --genomeFastaFiles {3} --sjdbGTFfile {4} --outTmpDir {6}"\
          " --sjdbOverhang {5}"\
          .format(
              path_star_software,
              star_threads,
              star_index_path_updated,
              ref_genome,
              annotation_path,
              star_index_read_length,
              tmp_path
    )
    printf('launching command: {0}'.format(cmd))

    exec_cmd(cmd, stdout)


if __name__ == "__main__":
    generate_star_index()
