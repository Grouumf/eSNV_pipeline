#! /usr/bin/python

from os.path import isfile
from os.path import isdir

from os import listdir

from os.path import getsize
from distutils.dir_util import mkpath
from sys import argv

from SNV_pipeline.config import FEATURE_COUNT
from SNV_pipeline.config import CUFFLINKS
from SNV_pipeline.config import ANNOTATION_PATH
from SNV_pipeline.config import MATRIX_OUTPUT_PATH as OUTPUT_PATH
from SNV_pipeline.config import OUTPUT_PATH_TOPHAT as TOPHAT_PATH
from SNV_pipeline.config import OUTPUT_PATH_STAR as STAR_PATH
from SNV_pipeline.config import STAR_THREADS
from SNV_pipeline.config import OUTPUT_PATH_MAPSPLICE as MAPSPLICE_PATH

from SNV_pipeline.server.single_cell_client import printf

from multiprocessing import Pool

from SNV_pipeline.bash_utils import exec_cmd

from time import time


############ VARIABLE ################
DEFAULT_ALIGNER = 'STAR'

if len(argv) > 1:
    DEFAULT_ALIGNER = argv[1]

OUTPUT_FILENAME = {
    'STAR': 'Aligned.sortedByCoord.out.bam',
    'tophat': 'accepted_hits.bam',
    'mapsplice': 'alignments.bam'
}

PATH_DICT = {
    'STAR': STAR_PATH ,
    'tophat': TOPHAT_PATH,
    'mapsplice': MAPSPLICE_PATH
}
######################################


def main():
    if DEFAULT_ALIGNER not in OUTPUT_FILENAME.keys():
        raise Exception('{0} not a regular aligner!'\
                        .format(DEFAULT_ALIGNER))

    aligner_path=PATH_DICT[DEFAULT_ALIGNER]
    output_filename = OUTPUT_FILENAME[DEFAULT_ALIGNER]

    do_expression_profile(aligner_path, output_filename)

def do_expression_profile(aligner_path, output_filename):
    """
    compute expression matrix according to aligner path results
    and output_filename (ex: output.bam)
    """
    cmd_list = []

    for folder in listdir(aligner_path):

        if not isdir(aligner_path + folder):
            printf('not a folder! continuing', folder)
            continue

        bam_file = "{0}/{1}/{2}"\
                   .format(aligner_path, folder, output_filename)

        if not isfile(bam_file):
            printf('no bam file for {0}'.format(bam_file))
            continue

        out_folder = "{0}/{1}/{2}"\
                   .format(OUTPUT_PATH, DEFAULT_ALIGNER, folder)

        out_file = '{0}/{1}'.format(out_folder, "matrix_count.txt")

        if isfile(out_file) and getsize(out_file):
            printf('expression matrix already exists for: {0}'.format(out_folder))
            continue

        cmd_list.append((bam_file, out_folder))

    nb_thread = input('number of threads:')
    assert(int(nb_thread))

    pool = Pool(int(nb_thread))
    pool.map(_multiprocess_func, cmd_list)

def _multiprocess_func(inp):
    """ """
    bam_file, out_folder = inp
    bam_file_to_expression_matrix(bam_file, out_folder)

def bam_file_to_expression_matrix(
        bam_file,
        out_folder,
        feature_count=FEATURE_COUNT,
        annotation_path=ANNOTATION_PATH,
        stdout=None,
        printf=printf,
        matrix_name="matrix_counts.txt"):
    """ """
    if not isdir(out_folder):
            mkpath(out_folder)

    cmd = "{0} -pPBCM --primary -T 1 -a {1} -o {2}/{4}"\
          " {3}".format(feature_count,
                        annotation_path,
                        out_folder,
                        bam_file,
                        matrix_name)
    printf('launching cmd: {0}\n'.format(cmd))
    try:
        exec_cmd(cmd, stdout)
    except Exception as e:
        printf('exception with featureCount cmd: {0}\n'.format(cmd))
        printf('exception: {0}\n'.format(e))

        assert(isfile('{0}/{1}'.format(out_folder, matrix_name)))

def cufflinks_analysis(
        bam_file,
        out_folder,
        cufflinks=CUFFLINKS,
        annotation_path=ANNOTATION_PATH,
        n_threads=STAR_THREADS,
        stdout=None,
        printf=printf):
    """ """
    if not isdir(out_folder):
            mkpath(out_folder)

    t_start = time()

    cmd = "{0} {1} -g {2} -o {3} -p {4} --no-update-check".format(
        cufflinks,
        bam_file,
        annotation_path,
        out_folder,
        n_threads)
    printf('launching cmd: {0}\n'.format(cmd))
    exec_cmd(cmd, stdout)

    with open('{0}/cufflinks_successfull.log'.format(out_folder), 'w') as f_log:
        f_log.write('done in {0} s'.format(time() - t_start))


if __name__ == "__main__":
    main()
