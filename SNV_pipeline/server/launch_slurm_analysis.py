from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD
from SNV_pipeline.server.server_config import CODE_ROOT
from SNV_pipeline.server.server_config import REMOTE_ELASTIC_SERVER
from SNV_pipeline.server.server_config import SSH_USER

from SNV_pipeline.config import CODE_ROOT as LOCAL_CODE_ROOT

from collections import defaultdict

import json

from os.path import split as pathsplit
from os import popen

import re

from glob import glob

from sys import argv


if '--new_only' in argv:
    NEW_ONLY = True
else:
    NEW_ONLY = False

if '--limit' in argv:
    LIMIT = int(argv[argv.index('--limit') + 1])
else:
    LIMIT = None

if '--min_samples' in argv:
    MIN_SAMPLES = int(argv[argv.index('--min_samples') + 1])
else:
    MIN_SAMPLES = None

if '--name' in argv:
    NAME = argv[argv.index('--name') + 1]
else:
    NAME = ''

if '--snv_analysis' in argv:
    ANALYSIS_NAME = 'process_everything_to_infer_eeSNVs'
else:
    ANALYSIS_NAME = 'process_everything_from_soft_files'


def main():
    """
    """
    regex_nb_of_samples = re.compile('GSE[0-9]+_n(?P<id>[0-9]*)_')

    cmd = "squeue -u opoirion|grep -P 'GSE[0-9]+' -o"

    gse_set = set(popen(cmd).read().split('\n'))

    for gse in gse_set:
        if not gse:
            continue
        print('current gse launched: {0}'.format(gse))

    array_launched = 0

    existing_gses = collect_existing_gses()

    for soft_file in glob('{0}/*.soft'.format(SOFT_FILE_TO_DOWNLOAD)):
        if NEW_ONLY:
            if gse in existing_gses:
                print('gse: {0} found in the database with synch gsm and' \
                      ' --new_only option. skipping...'.format(
                    gse))
                continue

        if NAME and not soft_file.count(NAME):
            continue

        soft_file = pathsplit(soft_file)[1]
        gse = re.findall('GSE[0-9]+', soft_file)[0]

        if MIN_SAMPLES:
            nb_sample = regex_nb_of_samples.findall(soft_file)

            if nb_sample:
                nb_sample = int(nb_sample[0])

                if nb_sample <= MIN_SAMPLES:
                    print('too few sample: {0} skipping'.format(nb_sample))

                    continue

        if gse in gse_set:
            print('{0} analysis seems to be already launched...skipping...'.format(gse))
            continue

        cmd = 'bash {0}/SNV_pipeline/SNV_pipeline/slurm/{1}.bash {2}'.format(
            CODE_ROOT,
            ANALYSIS_NAME,
            soft_file)
        print('cmd launched: {0}'.format(cmd))

        print(popen(cmd).read())

        array_launched += 1

        if LIMIT and array_launched >= LIMIT:
            break

def collect_existing_gses():
    """
    connect via ssh and collect all the GSE from the sc database
    for which we have at least one single cells synchronized
    """
    cmd = 'ssh {0}@{1} -t "python {2}/SNV_pipeline/SNV_pipeline/local/' \
          'collect_existing_gse.py"'.format(
              SSH_USER, REMOTE_ELASTIC_SERVER, LOCAL_CODE_ROOT)
    output = popen(cmd).read()

    try:
        results = json.loads(output)
    except Exception:
        print('check collect existing gses!\noutput: {0}\ncmd:{1}'.format(
            output, cmd))

        return False

    return results['buckets']

def check_gsm_status(gsm, infer_snv=False):
    """
    """
    cmd = 'ssh {0}@{1} -t "python {2}/SNV_pipeline/SNV_pipeline/local/' \
          'query_if_gsm_already_synchronised.py {3}"'.format(
        SSH_USER,
        REMOTE_ELASTIC_SERVER,
        LOCAL_CODE_ROOT, gsm)

    output = popen(cmd).read()

    try:
        results = json.loads(output)
    except Exception:
        print('check gsm status error!\noutput: {0}\ncmd:{1}'.format(
            output, cmd))

        return False

    results = defaultdict(bool, results)

    if not infer_snv and results['gene_fpkm_updated']:
        return True

    elif infer_snv and (results['raw_count_updated'] and \
                        results['snv_updated']):
        return True

    else:
        return False


if __name__ == '__main__':
    main()
