import unittest

from os import popen

from commands import getstatusoutput

from os.path import isfile
from os.path import isdir

from SNV_pipeline.server_config import OUTPUT_ROOT

from SNV_pipeline.server_config import TYPE_VAR

from SNV_pipeline.server_config import JAVA
from SNV_pipeline.server_config import GATK_DIR
from SNV_pipeline.server_config import PICARD_DIR
from SNV_pipeline.server_config import PATH_STAR_SOFTWARE
from SNV_pipeline.server_config import FASTQC
from SNV_pipeline.server_config import FEATURE_COUNT


class TestPackage(unittest.TestCase):
    """ """
    def test_output_root(self):
        """assert that OUTPUT_ROOT folder exists (SERVER SIDE)"""
        self.assertTrue(isdir(OUTPUT_ROOT))

    def test_ref_genome(self):
        """assert that ref genome file exists (SERVER SIDE)"""
        for typ in TYPE_VAR:
            self.assertTrue(isfile(TYPE_VAR[typ]['REF_GENOME']))

    def test_dbsnp(self):
        """assert that dbsnp vcf file exists (SERVER SIDE)"""
        for typ in TYPE_VAR:
            self.assertTrue(isfile(TYPE_VAR[typ]['DBSNP']))

    def test_vcf_resources(self):
        """assert that additional vcf file exist (SERVER SIDE)"""
        for typ in TYPE_VAR:
            for vcf in TYPE_VAR[typ]['VCF_RESOURCES']:
                self.assertTrue(isfile(vcf))

    def test_java(self):
        """assert that java is installed and > 1.8 (SERVER SIDE)"""
        res = getstatusoutput('{0} -version'.format(JAVA))[1]
        self.assertIsNotNone(res)

        version = res.split('"')[1].rsplit('.', 1)[0]
        self.assertTrue(float(version) >= 1.8)

    def test_GATK(self):
        """assert that GATK .jar file exists (SERVER SIDE)"""
        self.assertTrue(isfile('{0}/GenomeAnalysisTK.jar'.format(GATK_DIR)))

    def test_picard_tools(self):
        """assert that picard-tools .jar files exist (SERVER SIDE)"""
        self.assertTrue(isfile('{0}/picard.jar'.format(PICARD_DIR)))
        self.assertTrue(isfile('{0}/picard-lib.jar'.format(PICARD_DIR)))

    def test_STAR_aligner(self):
        """assert that STAR aligner bin exists and return version (SERVER SIDE)"""
        self.assertIsNotNone(popen('{0} --version'.format(PATH_STAR_SOFTWARE)))

    def test_featurecount(self):
        """assert that featureCounts is installed (SERVER SIDE)"""
        self.assertFalse(getstatusoutput("{0} -v".format(FEATURE_COUNT))[0])

    def test_fastqc(self):
        """assert that fastQC is installed (SERVER SIDE)"""
        self.assertFalse(getstatusoutput("{0} -version".format(FASTQC))[0])


if __name__ == "__main__":
    unittest.main()
