from SNV_pipeline.server.server_config import PORT
from SNV_pipeline.server.server_config import SQLITE_PATH
from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD
from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER

from SNV_pipeline.server.process_soft import extract_gsm_from_soft
from SNV_pipeline.server.source_variable_for_gse import create_slurm_file_for_gse

import threading
from threading import Thread

import SocketServer

from time import sleep

import sqlite3

from os import mkdir
from os.path import isdir

from glob import glob


################ LOCAL VARIABLE ################
HOST = 'localhost'
################################################


if not isdir(SQLITE_PATH):
    mkdir(SQLITE_PATH)


def launch_server():
    """
    """
    operational_server = OperationalServer()
    operational_server.run()

def main():
    """
    """
    msg_server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    msg_thread = Thread(target=msg_server.serve_forever)
    msg_thread.daemon = True

    operational_thread = Thread(target=launch_server)
    operational_thread.daemon = True

    operational_thread.start()
    msg_thread.start()

    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        print('\n#### keyboard interupt closing the server...')
        msg_server.shutdown()
        del operational_thread


class OperationalServer(object):
    """
    """
    def __init__(self):
        if not isdir(SOFT_FILE_TO_DOWNLOAD):
            mkdir(SOFT_FILE_TO_DOWNLOAD)

        self.cursor = None
        self._init_sqlite_db()

        print('#### server starting at {0}:{1}...'.format(HOST, PORT))

    def run(self):
        """
        """
        while True:
            self._scan_soft_files()
            self._scan_for_gse()
            sleep(20)

    def _scan_for_gse(self):
        """
        """
        proceeding_gse  = set(
            [gse for gse in self.cursor.execute('select * from gse_proceeding')])
        gse_list  = set(
            [gse for gse in self.cursor.execute('select * from gse_table')])

        for values in gse_list.difference(proceeding_gse):
            gse, ftp, organism, soft = values
            self.cursor.execute("""INSERT or IGNORE into gse_proceeding values
                                    ("{0}", "{1}", "{2}", "{3}") """.format(
                                        gse, ftp, organism, soft))

            gse, ftp, organism, soft = values
            self._init_gse_processing(gse, ftp, organism, soft)

        self.cursor.commit()

    def _init_gse_processing(self, gsm, ftp, geo_organism, soft):
        """
        """
        print('#### initialize GSM: {0}'.format(gsm))
        gsm_project = soft.split('.')[0]
        folder = '{0}/{1}'.format(PATH_OUTPUT_FOLDER, gsm_project)

        if not isdir(folder):
            mkdir(folder)

        path_gsm = '{0}/{1}'.format(folder, gsm)

        if not isdir(path_gsm):
            mkdir(path_gsm)

        create_slurm_file_for_gse(
            gsm=gsm,
            gsm_project=gsm_project,
            path_gsm=path_gsm,
            geo_organism=geo_organism,
            ftp=ftp,
            plateform='ILLUMINA',
            )

    def _scan_soft_files(self):
        """
        """
        existing_soft_name  = set([soft[0] for soft in
            self.cursor.execute('select * from soft_files')])

        soft_files = set(glob('{0}/*.soft'.format(SOFT_FILE_TO_DOWNLOAD)))
        soft_names = set([soft.split('/')[-1] for soft in soft_files])
        soft_names.difference_update(existing_soft_name)

        for soft_file in soft_files:
            soft_name = soft_file.split('/')[-1]
            soft_folder = soft_name.split('.')[0]

            if soft_name in existing_soft_name:
                continue

            print('new soft file found: {0}'.format(soft_name))

            soft_folder = '{0}/{1}'.format(PATH_OUTPUT_FOLDER, soft_folder)

            if not isdir(soft_folder):
                mkdir(soft_folder)

            gse_dict = extract_gsm_from_soft(soft_file)
            self.cursor.execute('INSERT or IGNORE into soft_files values ("{0}")'.format(
                soft_name))
            self.cursor.commit()

            self._process_gse_dict(gse_dict, soft_name)

    def _process_gse_dict(self, gse_dict, soft_name):
        """
        """
        existing_gse = [self.cursor.execute('select * from gse_table')]
        gse_list = []
        gse_error = []

        for gse in gse_dict:
            if gse not in existing_gse:
                if 'sra' not in gse_dict[gse]:
                    gse_error.append((gse, soft_name, '.sra not fount'))
                elif 'organism_ch1' not in gse_dict[gse]:
                    gse_error.append((gse, soft_name, 'no organism found'))
                elif  gse_dict[gse]['organism_ch1'][0] not in ['Mus musculus', 'Homo sapiens']:
                    gse_error.append((gse, soft_name, 'organism unknown'))
                else:
                    gse_list.append((gse, gse_dict[gse]['sra'], gse_dict[gse]['organism_ch1']))

        for gse, ftp, organism in gse_list:
            self.cursor.execute('''INSERT or IGNORE into gse_table values
                                    ("{0}", "{1}", "{2}", "{3}") '''.format(
                                        gse, ftp[0], organism[0], soft_name))
        for gse, error in gse_error:
            self.cursor.execute('''INSERT or IGNORE into gse_error values
                                ("{0}", "{1}", "{2}") '''.format(gse, error, soft_name))

        self.cursor.commit()

    def _init_sqlite_db(self):
        """
        """
        self.cursor = sqlite3.connect('{0}/sqlite_db.db'.format(SQLITE_PATH))
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS
                                soft_files (file text primary key)''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS gse_table
                                (gse text primary key, sra_ftp text, organism text, soft text)''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS gse_proceeding
                                (gse text primary key, sra_ftp text, organism text, soft text)''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS gse_finished
                                (gse text primary key, soft text)''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS gse_error
                                (gse text primary key, error text, soft text)''')

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    """
    """
    pass

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    """
    """
    def handle(self):
        """
        """
        self._receive_msg()

    def _receive_msg(self):
        """
        """
        data = self.request.recv(1024)
        print('$-> {0}'.format(data))
        cur_thread = threading.current_thread()
        response = "received {}: {}".format(cur_thread.name, data)
        self.request.sendall(response)


if __name__ == '__main__':
    main()
