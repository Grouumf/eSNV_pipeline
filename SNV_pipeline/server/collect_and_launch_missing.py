from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER
from SNV_pipeline.server.server_config import ARRAY_LIMIT
from SNV_pipeline.server.server_config import MAX_SIMULTANEOUS_ARRAY
from SNV_pipeline.server.server_config import CODE_ROOT

from os.path import split as pathsplit
from os.path import isdir

from os import popen
from os import mkdir

from glob import glob

from datetime import datetime

from collections import Counter

from SNV_pipeline.server.process_soft import extract_gsm_from_soft

from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD

from os.path import isfile

from sys import argv
from sys import stdout


PATH_MISSING_FOLDER = '{0}/missing_tsv/'.format(PATH_OUTPUT_FOLDER)


if not isdir(PATH_MISSING_FOLDER):
    mkdir(PATH_MISSING_FOLDER)


if '--max_project' in argv:
    MAX_PROJECT = int(argv[argv.index('--max_project') + 1])
else:
    MAX_PROJECT = 0

if '--max_gsm' in argv:
    MAX_GSM = int(argv[argv.index('--max_gsm') + 1])
else:
    MAX_GSM = 0


def main():
    """ """
    print('collecting all missing files...')

    folders = glob('{0}/*'.format(PATH_OUTPUT_FOLDER))
    missing_list = []
    counter = Counter()

    nb_soft = 0
    nb_gsm = 0

    for folder in folders:
        stdout.write('\rfolder processed: {0} ##############        '.format(
            folder))
        stdout.flush()

        soft_project = pathsplit(folder)[-1]
        soft_file = '{0}/{1}.soft'.format(
            SOFT_FILE_TO_DOWNLOAD,
            soft_project)

        if not isfile(soft_file):
            continue

        gse_dict = extract_gsm_from_soft(soft_file,path_soft='')

        if not len(gse_dict):
            continue

        nb_soft += 1

        for gsm in gse_dict:
            if isdir('{0}/{1}/{2}'.format(PATH_OUTPUT_FOLDER, soft_project, gsm)):
                continue

            sra = gse_dict[gsm]['SRA']

            if not gse_dict[gsm]['organism_code']:
                continue

            organism = gse_dict[gsm]['organism_code']

            counter[soft_file] += 1

            nb_gsm += 1

            missing_list.append([gsm, sra, organism, soft_project])

            if MAX_GSM and nb_gsm >= MAX_GSM:
                break

        if MAX_PROJECT and nb_soft >= MAX_PROJECT:
            break

    nb_missings = len(missing_list)
    print('\nnumber of missings: {0}'.format(nb_missings))

    if not nb_missings:
        print('no missing founds!')
        return

    for project in counter:
        print('number of missing for project {0}: {1}'.format(project, counter[project]))

    n_tsv = 1
    count = 0

    now = datetime.today().ctime().replace(' ', '_').split(':')[0]
    f_batch_name = '{0}/missing_n{1}_{2}.tsv'.format(PATH_MISSING_FOLDER, n_tsv, now)
    f_batch_list = []

    f_batch = open(f_batch_name, 'w')

    for experiment in missing_list:
        f_batch.write('\t'.join(experiment) + '\n')

        count += 1

        if count >= ARRAY_LIMIT:
            n_tsv += 1
            f_batch_name = '{0}/missing_n{1}_{2}.tsv'.format(PATH_MISSING_FOLDER, n_tsv, now)
            f_batch_list.append((f_batch_name, count))
            f_batch = open(f_batch_name, 'w')
            count = 0

    if count:
        f_batch_list.append((f_batch_name, count))

    for f_batch_name, size in f_batch_list:
        cmd = "sbatch --array=0-{size}%{max_simultaneous_array} --job-name MISSINGLIST " \
                               "{code_root}/SNV_pipeline/SNV_pipeline/slurm/" \
                               "process_everything_from_soft_files.slurm {file}".format(
                                   file=f_batch_name,
                                   size=size,
                                   max_simultaneous_array=MAX_SIMULTANEOUS_ARRAY,
                                   code_root=CODE_ROOT)
        print('cmd launched: {0}'.format(cmd))
        print(popen(cmd).read())


if __name__ == '__main__':
    main()
