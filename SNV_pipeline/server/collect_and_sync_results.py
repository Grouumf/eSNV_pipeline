from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER
from SNV_pipeline.server.server_config import PATH_LOCAL_DATABASE
from SNV_pipeline.server.server_config import HOME_SRV_ADR
from SNV_pipeline.server.server_config import PATH_LOCAL_SOFTFILE
from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD

from SNV_pipeline.bash_utils import exec_cmd

from datetime import datetime

from os import popen

from os.path import isfile

from os.path import split as pathsplit

from multiprocessing.pool import ThreadPool

######################## VARIABLE ############################
NB_THREAD = 2
###############################################################


def main():
    """ """
    print('syncronizing soft files...')
    popen('rsync -rv {0} {1}:{2}'.format(
        SOFT_FILE_TO_DOWNLOAD, HOME_SRV_ADR, PATH_LOCAL_SOFTFILE)).read()

    print('collecting successfull logs...')
    success_list = popen('find {0} -name "*pipeline_successfull.log"'.format(
        PATH_OUTPUT_FOLDER)).read().split()

    print('number of success: {0}'.format(len(success_list)))

    pool = ThreadPool(NB_THREAD)
    nb_syncronized= len(pool.map(synchronize_with_local, success_list))

    print('number of samples syncronized: {0}'.format(nb_syncronized))

def synchronize_with_local(
        success,
        path_local_database=PATH_LOCAL_DATABASE):
    """
    """
    path = pathsplit(success)[0]
    path_log = '{0}/syncronised_successful.log'.format(path)

    if isfile(path_log):
        return 0

    exec_cmd('rsync -rv {0} {1}:{2}'.format(
        path, HOME_SRV_ADR, path_local_database))

    with open(path_log, 'w') as f_log:
        f_log.write('syncronized: {0}'.format(datetime.today().ctime()))

    print('{0} syncronized!'.format(success))
    return 1


if __name__ == '__main__':
    main()
