from SNV_pipeline.server.download_sra_utils import download_sra
from SNV_pipeline.server.download_sra_utils import extract_fast_from_sra
from SNV_pipeline.server.download_sra_utils import compute_av_read_length

from SNV_pipeline.deploy_fastq_aligner.deploy_star import star_analysis
from SNV_pipeline.deploy_fastq_aligner.deploy_star import clean_star_folder
from SNV_pipeline.deploy_fastq_aligner.deploy_star import check_star_folder

from SNV_pipeline.process_freebayes import ProcessFreebayesCaller
from SNV_pipeline.generate_STAR_genome_index import generate_star_index
from SNV_pipeline.compute_frequency_matrix import bam_file_to_expression_matrix
from SNV_pipeline.compute_frequency_matrix import cufflinks_analysis

from SNV_pipeline.server.server_config import PICARD_DIR
from SNV_pipeline.server.server_config import GATK_DIR
from SNV_pipeline.server.server_config import PATH_STAR_SOFTWARE
from SNV_pipeline.server.server_config import FEATURE_COUNT
from SNV_pipeline.server.server_config import STAR_THREADS
from SNV_pipeline.server.server_config import TYPE_VAR
from SNV_pipeline.server.server_config import CUFFLINKS
from SNV_pipeline.server.server_config import JAVA
from SNV_pipeline.server.server_config import SAMTOOLS
from SNV_pipeline.server.server_config import PATH_FREEBAYES
from SNV_pipeline.server.server_config import PATH_LOCAL_DATABASE

from SNV_pipeline.server.pipeline_utils import clean_sra_bam_fastq_files
from SNV_pipeline.server.pipeline_utils import _wait_STAR_index_compiling
from SNV_pipeline.server.pipeline_utils import check_cufflinks
from SNV_pipeline.server.pipeline_utils import check_gatk_vcf_file
from SNV_pipeline.server.pipeline_utils import check_expression_matrix

from SNV_pipeline.server.collect_and_sync_results import synchronize_with_local

from SNV_pipeline.server.single_cell_client import SSHClient

from SNV_pipeline.server.launch_slurm_analysis import check_gsm_status

from sys import argv

from os import mkdir
from os.path import isdir
from os.path import isfile

from os import popen
from os import remove

from glob import glob

from shutil import rmtree
from shutil import copyfile

import json

from SNV_pipeline.bash_utils import exec_cmd

from time import time

import traceback


################  GLOBAL VARIABLE ##############
if "--fastq" not in argv:
    assert("--url" in argv)
    URL = argv[
        argv.index("--url") + 1]
    FASTQ = None
    assert(URL[:2] != '--')
else:
    URL = None
    FASTQ = argv[
        argv.index("--fastq") + 1]

assert("--gsm" in argv)
GSM = argv[
    argv.index("--gsm") + 1]

assert(GSM[0].count('-') == 0)

assert("--gsm_project" in argv)
GSM_PROJECT = argv[
    argv.index("--gsm_project") + 1]

assert(GSM_PROJECT[:2] != '--')

assert("--organism" in argv)
ORGANISM = argv[
    argv.index("--organism") + 1]

assert(ORGANISM[:2] != '--')

if '--infer_snv' in argv:
    INFER_SNV = True
else:
    INFER_SNV = False

if '--freebayes_only' in argv:
    FREEBAYES_ONLY = True
else:
    FREEBAYES_ONLY = False

if '--skip_check' in argv:
    SKIP_CHECK = True
else:
    SKIP_CHECK = False

if '--do_cufflinks' in argv:
    DO_CUFFLINKS = True
else:
    DO_CUFFLINKS = False

if '--do_feature_count' in argv:
    DO_FEATURE_COUNT = True
else:
    DO_FEATURE_COUNT = False

if '--rm_tmp_dir' in argv:
    RM_TMP_DIR = True
else:
    RM_TMP_DIR = False

if '--keep_bam_file' in argv:
    KEEP_BAM_FILE = True
else:
    KEEP_BAM_FILE = False

if '--sync_to' in argv:
    SYNC_TO = argv[
    argv.index("--sync_to") + 1]
else:
    SYNC_TO = PATH_LOCAL_DATABASE

if '--path_output_folder' in argv:
    PATH_OUTPUT_FOLDER = argv[
        argv.index("--path_output_folder") + 1]
else:
    from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER


PATH_GSM = '{0}/{1}/'.format(PATH_OUTPUT_FOLDER, GSM_PROJECT)
PATH_TO_GSM_FOLDER = '{0}/{1}/{2}/'.format(PATH_OUTPUT_FOLDER, GSM_PROJECT, GSM)

STAR_INDEX_PATH = TYPE_VAR[ORGANISM]['STAR_INDEX_PATH']
PLATEFORM = 'ILLUMINA'
REF_GENOME = TYPE_VAR[ORGANISM]['REF_GENOME']

if '--ref_genome' in argv:
    REF_GENOME = argv[
        argv.index("--ref_genome") + 1]

if '--star_index_path' in argv:
    STAR_INDEX_PATH_UPDATED = argv[
        argv.index("--star_index_path") + 1]
else:
    STAR_INDEX_PATH_UPDATED = None

if '--read_length' in argv:
    READ_LENGTH = argv[
        argv.index("--read_length") + 1]
else:
    READ_LENGTH = None

DBSNP = TYPE_VAR[ORGANISM]['DBSNP']
VCF_RESOURCES = TYPE_VAR[ORGANISM]['VCF_RESOURCES']

ANNOTATION_PATH = TYPE_VAR[ORGANISM]['ANNOTATION_PATH']

DEBUG = True if "--debug" in argv else False
################################################


def main():
    """
    """
    if not isdir(PATH_GSM):
        mkdir(PATH_GSM)

    if not isdir(PATH_TO_GSM_FOLDER):
        mkdir(PATH_TO_GSM_FOLDER)

    ssh_client = SSHClient(log_folder=PATH_TO_GSM_FOLDER)
    error_log = '{0}/pipeline_error.log'.format(PATH_TO_GSM_FOLDER)

    if isfile(error_log):
        remove(error_log)

    if DEBUG:
        pipeline(ssh_client)
    else:
        try:
            pipeline(ssh_client)

        except Exception as e:
            if e == KeyboardInterrupt:
                del ssh_client
                print('exiting...')
                return

            trace = traceback.format_exc()

            ssh_client.send_msg(
                """
                ################
                Exception found for gsm: {0} from project {1}
                msg: {2} at:\n####\n {3} \n####\n
                ################
                """.format(GSM, GSM_PROJECT, e, trace))

            log_file = open(error_log, 'w')
            log_file.write(json.dumps(
                {
                    'project': GSM_PROJECT,
                    'url': URL,
                    'organism': ORGANISM,
                    'gsm': GSM,
                    'error': trace,
                }, indent=2))

            del ssh_client


def pipeline(ssh_client, infer_snv=INFER_SNV):
    """ """
    start_time = time()

    ssh_client.send_msg('''
    ################
    start pipeline for:
            GSM: {0}
            URL: {1}
            PROJECT: {2}
            ORGANISM: {3}
    ################\n
    '''.format(GSM, URL, GSM_PROJECT, ORGANISM))

    if not SKIP_CHECK:
        if check_gsm_status(GSM, infer_snv):
            ssh_client.send_msg('gsm: {0} already found in the database and' \
                                ' synchronized return...'.format(GSM))
            return

    if not FASTQ:
        ssh_client.send_msg(
            '#### downloading and extracting the SRA file for {0}... ####'.format(
                GSM))

        msg = download_sra(url_address=URL,
                           gsm=GSM,
                           path_to_download=PATH_TO_GSM_FOLDER,
                           stdout=ssh_client.trace_file,
                           printf=ssh_client.send_msg)
        ssh_client.send_msg(msg)

        ssh_client.send_msg('Final memory: after downloading SRA...')
        ssh_client.send_msg(popen('free -g').read())

        star_index_read_length, msg = extract_fast_from_sra(
            stdout=ssh_client.trace_file,
            printf=ssh_client.send_msg,
            gsm=GSM, path_gsm=PATH_TO_GSM_FOLDER)
        ssh_client.send_msg(msg)

        ssh_client.send_msg('Final memory: after extracting SRA...')
        ssh_client.send_msg(popen('free -g').read())

    else:
        for fastq in glob(FASTQ):
            new_fastq = '{0}/{1}/{2}'.format(
                PATH_GSM, GSM, fastq.split('/')[-1])

            if not isfile(new_fastq):
                copyfile(fastq, new_fastq)

    if READ_LENGTH:
        star_index_read_length = READ_LENGTH
    else:
        star_index_read_length = compute_av_read_length(FASTQ)

    if STAR_INDEX_PATH_UPDATED:
        star_index_path_updated = STAR_INDEX_PATH_UPDATED
    else:
        star_index_path_updated = "{0}READ{1}".format(
            STAR_INDEX_PATH.rstrip('/'),
            star_index_read_length)

    if not isdir(star_index_path_updated):
        ssh_client.send_msg('#### creating {0} STAR index...'.format(star_index_path_updated))
        generate_star_index(
            star_index_path=STAR_INDEX_PATH,
            star_threads=STAR_THREADS,
            ref_genome=REF_GENOME,
            annotation_path=ANNOTATION_PATH,
            star_index_read_length=star_index_read_length,
            path_star_software=PATH_STAR_SOFTWARE,
            printf=ssh_client.send_msg,
            stdout=ssh_client.trace_file,
            simulated_ref_genome=None)

        ssh_client.send_msg('Final memory: after creating STAR index...')
        ssh_client.send_msg(popen('free -g').read())

        assert(isfile('{0}/SA'.format(star_index_path_updated)))
        assert(isfile('{0}/SAindex'.format(star_index_path_updated)))

    else:
        if not  _wait_STAR_index_compiling(star_index_path_updated, ssh_client):
            tmp_path_star_index = PATH_TO_GSM_FOLDER + '/STARindex/'
            tmp_path_star_index_updated = tmp_path_star_index.rstrip('/') + 'READ{0}'.format(
                star_index_read_length)

            if isdir(tmp_path_star_index_updated):
                popen('rm -r {0}'.format(star_index_path_updated)).read()

            generate_star_index(
                star_index_path=tmp_path_star_index,
                star_threads=STAR_THREADS,
                ref_genome=REF_GENOME,
                annotation_path=ANNOTATION_PATH,
                star_index_read_length=star_index_read_length,
                path_star_software=PATH_STAR_SOFTWARE,
                printf=ssh_client.send_msg,
                stdout=ssh_client.trace_file,
                simulated_ref_genome=None)

            ssh_client.send_msg('Final memory: after creating STAR index...')
            ssh_client.send_msg(popen('free -g').read())

            if not ( isfile('{0}/SA'.format(star_index_path_updated)) \
                     and  isfile('{0}/SAindex'.format(star_index_path_updated)) ):
                if isdir(star_index_path_updated):
                    popen('rm -r {0}'.format(star_index_path_updated)).read()
                    exec_cmd('mv {0} {1}'.format(
                        tmp_path_star_index_updated, star_index_path_updated))
            else:
                popen('rm -r {0}'.format(tmp_path_star_index_updated)).read()

    ############ STAR ANALYSIS ####################################
    star_check = check_star_folder(PATH_TO_GSM_FOLDER)

    if star_check:
        ssh_client.send_msg(star_check)
    else:
        ssh_client.send_msg('#### launch star analysis for: {0}... ####'.format(GSM))
        star_analysis(
            output_path=PATH_GSM,
            fastq_path=PATH_GSM,
            pattern=GSM,
            cufflinks_compatibility=True,
            star_index_path=star_index_path_updated,
            star_index_read_length=star_index_read_length,
            printf=ssh_client.send_msg,
            stdout=ssh_client.trace_file,
            simulated_ref_genome=None,
            custom_star_index_name=False,
            path_software=PATH_STAR_SOFTWARE, threads=STAR_THREADS)
        clean_star_folder(PATH_TO_GSM_FOLDER)

        ssh_client.send_msg('Final memory: after STAR analysis...')
        ssh_client.send_msg(popen('free -g').read())

    ############ READ COUNT ANALYSIS ##############################
    if DO_FEATURE_COUNT:
        ssh_client.send_msg('#### launching feature count analysis...')
        matrix_check = check_expression_matrix(PATH_TO_GSM_FOLDER)

        if matrix_check:
            ssh_client.send_msg(matrix_check)
        else:
            bam_file_to_expression_matrix(
                feature_count=FEATURE_COUNT,
                annotation_path=ANNOTATION_PATH,
                bam_file=PATH_TO_GSM_FOLDER + '/Aligned.sortedByCoord.out.bam',
                out_folder=PATH_TO_GSM_FOLDER,
                printf=ssh_client.send_msg,
                stdout=ssh_client.trace_file)

    if DO_CUFFLINKS:
        ssh_client.send_msg('#### launching cufflinks analysis...')
        cuff_check = check_cufflinks(PATH_TO_GSM_FOLDER)

        if cuff_check:
            ssh_client.send_msg(cuff_check)
        else:
            cufflinks_analysis(
                bam_file=PATH_TO_GSM_FOLDER + '/Aligned.sortedByCoord.out.bam',
                out_folder=PATH_TO_GSM_FOLDER,
                annotation_path=ANNOTATION_PATH,
                n_threads=STAR_THREADS,
                printf=ssh_client.send_msg,
                stdout=ssh_client.trace_file,
                cufflinks=CUFFLINKS)

            ssh_client.send_msg('Final memory: after cufflinks analysis...')
            ssh_client.send_msg(popen('free -g').read())

    ############ GATK SNV ANALYSIS ################################
    if infer_snv:
        vcf_check = check_gatk_vcf_file(PATH_TO_GSM_FOLDER)

        if vcf_check:
            ssh_client.send_msg(vcf_check)
        else:
            ssh_client.send_msg(
                '#### launch Freebayes + GATK analysis for: {0}... ####'.format(GSM))
            process_gatk_snv = ProcessFreebayesCaller(
                output_path=PATH_TO_GSM_FOLDER,
                output_path_gatk=PATH_TO_GSM_FOLDER + '/GATK/',
                respath=PATH_TO_GSM_FOLDER,
                respath_gatk=PATH_TO_GSM_FOLDER,
                bam_file_name='Aligned.sortedByCoord.out.bam',
                path_to_data=PATH_TO_GSM_FOLDER,
                picard_dir=PICARD_DIR,
                gatk_dir=GATK_DIR,
                plateform=PLATEFORM,
                samtools=SAMTOOLS,
                path_freebayes=PATH_FREEBAYES,
                java=JAVA,
                organism=ORGANISM,
                ref_genome=REF_GENOME,
                dbsnp=DBSNP,
                vcf_resources=VCF_RESOURCES,
                clean_tmp=False,
            )

            if FREEBAYES_ONLY:
                process_gatk_snv.process(GSM)
            else:
                process_gatk_snv.process_ALL_callers(GSM)

            with open('{0}/snv_inferrence_successfull.log'.format(
                    PATH_TO_GSM_FOLDER), 'w') as f_log:
                f_log.write('snv called in {0} s'.format(
                    time() - process_gatk_snv.time_start))

    ############ REMOVE DATA FILES ###############################
    clean_sra_bam_fastq_files(PATH_TO_GSM_FOLDER,
                              printf=ssh_client.send_msg,
                              keep_bam_file=KEEP_BAM_FILE)

    end_time = time() - start_time

    log_file = open('{0}/pipeline_successfull.log'.format(PATH_TO_GSM_FOLDER), 'w')

    if SYNC_TO:
        ssh_client.send_msg('be sure that the local path exists: {0}'.format(
            SYNC_TO))

    synchronize_with_local(
        '{0}/pipeline_successfull.log'.format(PATH_TO_GSM_FOLDER),
        SYNC_TO)

    log_file.write('pipeline finished in {0} s'.format(end_time))
    ssh_client.send_msg('#### process finished in {0} s! ####'.format(end_time))

    ssh_client.send_msg('Final memory: after EVERYTHING analysis...')
    ssh_client.send_msg(popen('free -g').read())

    if RM_TMP_DIR:
        rmtree(PATH_TO_GSM_FOLDER)
        ssh_client.send_msg('temp folder: {0} removed'.format(
            PATH_TO_GSM_FOLDER))

    return


if __name__ == '__main__':
    main()
