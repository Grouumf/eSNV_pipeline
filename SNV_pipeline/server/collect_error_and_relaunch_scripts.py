from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER
from SNV_pipeline.server.server_config import ARRAY_LIMIT
from SNV_pipeline.server.server_config import MAX_SIMULTANEOUS_ARRAY
from SNV_pipeline.server.server_config import CODE_ROOT

from os.path import split as pathsplit
from os.path import isdir

from os import popen
from os import mkdir

from sys import argv

import json

import re

from datetime import datetime

from collections import Counter

from sys import stdout

if '--name' in argv:
    NAME = argv[argv.index('--name') + 1]
else:
    NAME = ''

if '--snv_analysis' in argv:
    ANALYSIS_NAME = 'process_everything_to_infer_eeSNVs'
else:
    ANALYSIS_NAME = 'process_everything_from_soft_files'


PATH_ERROR_FOLDER = '{0}/error_tsv/'.format(PATH_OUTPUT_FOLDER)


if not isdir(PATH_ERROR_FOLDER):
    mkdir(PATH_ERROR_FOLDER)


def main():
    """ """
    print('collecting all error files...')
    error_list = popen('find {0} -name "*error.log"'.format(
        PATH_OUTPUT_FOLDER)).read().split()

    nb_errors = len(error_list)
    print('number of errors: {0}'.format(nb_errors))

    if not nb_errors:
        print('no error founds!')
        return

    error_batch = []
    counter = Counter()
    count = -1

    for error in error_list:

        count += 1
        stdout.write('\r{0} / {1} error done...'.format(count, nb_errors))
        stdout.flush()

        error_json = json.loads(open(error).read())
        gsm = error_json['gsm']
        project = error_json['project']

        if NAME:
            if not (gsm.count(NAME) or project.count(NAME)):
                continue

        if 'organism' not in error_json or 'url' not in error_json:
            log = open(pathsplit(error)[0] + '/log_file.log').read(300)
        else:
            log = None

        if 'organism' not in error_json:
            organism = re.findall('ORGANISM: (?P<organism>[A-Z]+)', log)[0]
        else:
            organism = error_json['organism']

        if 'url' not in error_json:
            url = re.findall('URL: (?P<organism>http.+)', log)[0]
        else:
            url = error_json['url']

        counter[project] += 1
        error_batch.append((gsm, url, organism, project))

    for project in counter:
        print('number of error for project {0}: {1}'.format(project, counter[project]))

    n_tsv = 1
    count = 0

    now = datetime.today().ctime().replace(' ', '_').split(':')[0]
    f_batch_name = '{0}/{3}error_n{1}_{2}.tsv'.format(
        PATH_ERROR_FOLDER, n_tsv, now, NAME)
    f_batch_list = []

    f_batch = open(f_batch_name, 'w')

    for experiment in error_batch:
        f_batch.write('\t'.join(experiment) + '\n')

        count += 1

        if count >= ARRAY_LIMIT:
            n_tsv += 1
            f_batch_name = '{0}/{3}error_n{1}_{2}.tsv'.format(
                PATH_ERROR_FOLDER, n_tsv, now, NAME)
            f_batch_list.append((f_batch_name, count))
            f_batch = open(f_batch_name, 'w')
            count = 0

    if count:
        f_batch_list.append((f_batch_name, count))

    for f_batch_name, size in f_batch_list:
        cmd = "sbatch --array=0-{size}%{max_simultaneous_array} --job-name ERRORLIST " \
                               "{code_root}/SNV_pipeline/SNV_pipeline/slurm/" \
                               "{analysis}.slurm {file}".format(
                                   file=f_batch_name,
                                   size=size,
                                   max_simultaneous_array=MAX_SIMULTANEOUS_ARRAY,
                                   code_root=CODE_ROOT,
                                   analysis=ANALYSIS_NAME)
        print('cmd launched: {0}'.format(cmd))
        print(popen(cmd).read())


if __name__ == '__main__':
    main()
