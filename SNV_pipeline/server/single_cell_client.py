from SNV_pipeline.server.server_config import PORT
from SNV_pipeline.server.server_config import HOST

import socket


class SSHClient(object):
    """
    """
    def __del__(self):
        """
        """
        if self.log_file is not None:
            self.log_file.close()

        if self.trace_file is not None:
            self.trace_file.close()

        if self.server is not None:
            self.server.close()

    def __init__(self, ip=HOST,
                 port=PORT,
                 log_folder=None,
                 desactivate=True):
        """
        """

        self.log_file = None
        self.trace_file = None

        if log_folder:
            self.log_file = open(log_folder + '/log_file.log', 'w')
            self.trace_file = open(log_folder + '/cmd_trace.log', 'w')

        self.desactivate = desactivate

        try:
            from sshtunnel import SSHTunnelForwarder

            self.server = SSHTunnelForwarder(
                ip,
                ssh_username="opoirion",
                ssh_password=''.join(chr(97) for _ in range(4)),
                remote_bind_address=('127.0.0.1', port)
            )

            self.server.start()
            self.local_port = self.server.local_bind_port
            self.ip = ip
            self.port = port

        except Exception:
            self.server = None
            self.local_port = None
            self.ip = None
            self.port = None

            print('#### unable to do a ssh port forwarding')

    def send_msg(self, message):
        """
        """
        if not self.desactivate:
            self._send_msg_through_ssh_socket(message)

        print(message)

        if self.log_file is not None:
            self.log_file.write(message + '\n')
            self.log_file.flush()

    def _send_msg_through_ssh_socket(self, message):
        """
        """
        if self.server is not None:
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect(('127.0.0.1', self.local_port))
            except Exception:
                return 1

            try:
                sock.sendall(message)
                sock.recv(1024)
            except Exception:
                return 1

            finally:
                sock.close()

def printf(msg):
    """
    """
    print(msg)


if __name__ == "__main__":
    # Port 0 means to select an arbitrary unused port
    client = SSHClient()

    client.send_msg('hello test 1')
    client.send_msg('hello test 2')
    client.send_msg('hello test 3')
    del client
