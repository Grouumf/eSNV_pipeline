from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD
from SNV_pipeline.server.server_config import ARRAY_LIMIT



from collections import defaultdict

from sys import argv

from os.path import isdir
from os.path import isfile

from datetime import datetime

import re

from os import mkdir


def main():
    """ """
    from SNV_pipeline.server.server_config import PATH_OUTPUT_FOLDER

    assert('--soft_file' in argv)

    soft_file = argv[argv.index('--soft_file') + 1]
    gsm = soft_file.rsplit('.', 1)[0]

    path_results = '{0}/{1}'.format(PATH_OUTPUT_FOLDER, gsm)

    if not isdir(path_results):
        mkdir(path_results)

    gsm_dict = extract_gsm_from_soft(soft_file)
    save_gsm_to_download(gsm_dict, path_results, gsm_project=soft_file)

def extract_gsm_from_soft(
        soft_file,
        path_soft=SOFT_FILE_TO_DOWNLOAD,
        flatten_gsm=False,
        remove_not_sra=True):
    """
    """
    gse_dict = {}
    soft_path = '{0}/{1}'.format(path_soft, soft_file)

    assert(isfile(soft_path))

    f_soft = open(soft_path)
    line = f_soft.readline()

    while line:
        if line.count('^SAMPLE'):
            data = defaultdict(list)
            gse = line.strip('\n').split(' = ', 1)[1].strip()
            data['GSE'] = gse

            line = f_soft.readline()

            while line and line[0] == '!':
                key, value = line.split(' = ', 1)

                key = key.strip('! ')
                key = key.replace('/', '_')
                value = value.strip('\n ')

                if key[:7] == 'Sample_':
                    key = key[7:]

                if value[:6] == 'ftp://':
                    data['ftp'].append(value)

                sra = re.findall('https://www.ncbi.nlm.nih.gov/sra?term=SRX[0-9]+', value)

                geo_organism = data['organism_ch1']

                if geo_organism:
                    geo_organism = geo_organism[0]

                    if geo_organism == 'Homo sapiens':
                        data['organism_code'] = 'HUMAN'
                    elif geo_organism == 'Mus musculus':
                        data['organism_code'] = 'MOUSE'

                if sra:
                    data['SRA'].append(sra[0])

                data[key].append(value)

                line = f_soft.readline()

            if 'relation' in data:
                for relation in data['relation']:
                    key, value = relation.split(': ')
                    key.strip(), value.strip()
                    data[key] = value

            if flatten_gsm:
                for key in data:
                    data[key] = check_value(key, data[key])

                    if len(data[key]) == 1:
                        data[key] = data[key][0]

            if 'SRA' in data or not remove_not_sra:
                gse_dict[gse] = data

        else:
            line = f_soft.readline()

    return gse_dict

def check_value(key, values):
    """
    """
    is_list= True

    if not isinstance(values, list):
        is_list = False
        values = [values]

    values = map(format_value, values)

    if key.count('zip') and not isinstance(values[0], int):
        values = map(lambda x:0, values)

    if key.count('phone') and not isinstance(values[0], int):
        values = map(lambda x:0, values)

    if not is_list:
        values = values[0]

    return values

def format_value(value):
    """
    """
    if value.isdigit():
        value = int(value)
    elif re.findall('[A-Z][a-z][a-z] [0-9]{2} [0-9]{4}', value):
        value = re.findall('[A-Z][a-z][a-z] [0-9]{2} [0-9]{4}', value)[0]
        value = datetime.strptime(value, '%b %d %Y')

    return value

def extract_gse_from_soft(
        soft_file,
        path_soft=SOFT_FILE_TO_DOWNLOAD):
    """
    """
    soft_path = '{0}/{1}'.format(path_soft, soft_file)
    assert(isfile(soft_path))

    f_soft = open(soft_path)
    line = ''
    gse = defaultdict(list)

    while not line.count('^SAMPLE'):
        line = f_soft.readline()

        while not line.count('^SAMPLE') and line[0] in ['!', '^']:
            if not line.count('='):
                line = f_soft.readline()
                continue

            key, value = line.split(' = ', 1)

            key = key.strip('! ^')
            key = key.replace('/', '_')

            value = value.strip('\n ')

            if key == 'SERIES':
                gse['gse'] = value

            gse[key].append(value)

            line = f_soft.readline()

        if 'relation' in gse:
            for relation in gse['relation']:
                key, value = relation.split(': ')
                key.strip(), value.strip()
                gse[key] = value

    for key in gse:
        gse[key] = check_value(key, gse[key])

        if len(gse[key]) == 1:
            gse[key] = gse[key][0]

    return gse

def save_gsm_to_download(gsm_dict, path_to_folder, gsm_project, array_limit=ARRAY_LIMIT):
    """
    parse gsm into ready to launch batch files
    """
    gsm_project = gsm_project.replace('.soft', '')
    nbatch = 1
    count = 0

    path_file = '{0}/gsm_to_download_batch{1}.tsv'.format(path_to_folder, nbatch)
    f_gsm = open(path_file, 'w')

    for gsm in gsm_dict:
        geo_organism = gsm_dict[gsm]['organism_ch1'][0]

        if geo_organism == 'Homo sapiens':
            cell_type = 'HUMAN'
        elif geo_organism == 'Mus musculus':
            cell_type = 'MOUSE'
        else:
            continue

        f_gsm.write('{0}\t{1}\t{2}\t{3}\n'.format(
            gsm, gsm_dict[gsm]['SRA'], cell_type, gsm_project))

        count += 1

        if count >= array_limit:
            print('{0} written'.format(path_file))
            nbatch += 1
            count = 0
            path_file = '{0}/gsm_to_download_batch{1}.tsv'.format(path_to_folder, nbatch)
            f_gsm = open(path_file, 'w')

    print('{0} written'.format(path_file))


if __name__ == '__main__':
    main()
