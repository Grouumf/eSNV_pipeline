from time import sleep
from glob import glob
from os.path import isfile
from os import remove


from SNV_pipeline.server.single_cell_client import printf



def clean_sra_bam_fastq_files(path_to_gsm_folder,
                              printf=printf,
                              keep_bam_file=False):
    """
    """
    for fil in glob('{0}/*.fastq'.format(path_to_gsm_folder)):
        printf('file to remove: {0}'.format(fil))
        remove(fil)

    for fil in glob('{0}/*.sra'.format(path_to_gsm_folder)):
        printf('file to remove: {0}'.format(fil))
        remove(fil)

    if not keep_bam_file:
        for fil in glob('{0}/STAR/*.bam'.format(path_to_gsm_folder)):
            printf('file to remove: {0}'.format(fil))
            remove(fil)
        for fil in glob('{0}/*.bam'.format(path_to_gsm_folder)):
            printf('file to remove: {0}'.format(fil))
            remove(fil)

    for fil in glob('{0}/*.gtf'.format(path_to_gsm_folder)):
        printf('file to remove: {0}'.format(fil))
        remove(fil)
    for fil in glob('{0}/tmp/*.gtf'.format(path_to_gsm_folder)):
        printf('file to remove: {0}'.format(fil))
        remove(fil)


def _wait_STAR_index_compiling(star_index_path_updated, ssh_client):
    """
    """
    if not ( isfile('{0}/SA'.format(star_index_path_updated)) \
     and  isfile('{0}/SAindex'.format(star_index_path_updated)) ):

        max_time = 520
        time = 0

        while not ( isfile('{0}/SA'.format(star_index_path_updated)) \
         and isfile('{0}/SAindex'.format(star_index_path_updated)) ) \
         and time < max_time:

            ssh_client.send_msg(
                '#### {0} index might have not finished to compile...waiting... {1} s'.format(
                    star_index_path_updated, time))

            sleep(120)
            time += 120

        if time > max_time:
            ssh_client.send_msg('''#### {0} index not detected after {1} s.
            Creating personnal index '''.format(
                star_index_path_updated, max_time))
            return False
    else:
        ssh_client.send_msg('#### {0} index detected !'.format(
            star_index_path_updated))
        return True

def check_gatk_vcf_file(gatk_vcf_folder):
    """
    """
    if isfile('{0}/snv_inferrence_successfull.log'.format(
            gatk_vcf_folder)):
        return '#### snvs already found in: {0}'.format(
            gatk_vcf_folder)

def check_expression_matrix(gsm_folder):
    """
    """
    if isfile('{0}/matrix_counts.txt'.format(gsm_folder)):
        return '#### matrix_counts.txt already found in: {0}'.format(gsm_folder)

def check_cufflinks(gsm_folder):
    """
    """
    if isfile('{0}/cufflinks_successfull.log'.format(gsm_folder)):
        return '#### cufflinks_successfull.log already found in: {0}'.format(gsm_folder)
