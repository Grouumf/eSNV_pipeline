"""prepare global variables for a given GSE project for bash sbatch scripts """

from SNV_pipeline.server.server_config import TYPE_VAR
from SNV_pipeline.server.server_config import CODE_ROOT
from SNV_pipeline.server.server_config import PATH_GSM_PROJECTS


def create_slurm_file_for_gse(
        gsm,
        gsm_project,
        path_gsm,
        geo_organism,
        ftp,
        plateform='ILLUMINA'):
    """
    """
    if geo_organism == 'Homo sapiens':
        cell_type = 'HUMAN'
    elif geo_organism == 'Mus musculus':
        cell_type = 'MOUSE'
    else:
        raise Exception('wrong organism: {0}'.format(geo_organism))

    ref_genome = TYPE_VAR[cell_type]['REF_GENOME']
    organism = TYPE_VAR[cell_type]['ORGANISM']
    dbsnp = TYPE_VAR[cell_type]['DBSNP']
    vcf_resources = TYPE_VAR[cell_type]['VCF_RESOURCES']
    annotation_path = TYPE_VAR[cell_type]['ANNOTATION_PATH']
    star_index_path = TYPE_VAR[cell_type]['STAR_INDEX_PATH']
    path_gsm_target = '{0}/{1}/{2}'.format(PATH_GSM_PROJECTS, gsm_project, gsm)


    path_slurm = '{0}/launch_pipeline.slurm'.format(path_gsm)

    f_slurm = open(path_slurm, 'w')

    f_slurm.write("""
#!/bin/bash

#SBATCH --job-name={0}_{1}
#SBATCH --partition=community.q

## 3 day max run time for community.q, kill.q, exclusive.q, and htc.q. 1 Hour max run time for sb.q

#SBATCH --time=2-23:59:59 ## time format is DD-HH:MM:SS

## task-per-node x cpus-per-task should not typically exceed core count on an individual node

#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=10
#SBATCH --mem=110000 ## max amount of memory per node you require
#SBATCH --error={2}/buzz-%A_%a.err ## %A - filled with jobid. %a - filled with job arrayid
#SBATCH --output={2}/buzz-%A_%a.out ## %A - filled with jobid. %a - filled with job arrayid

## Useful for remote notification
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,TIME_LIMIT_80
#SBATCH --mail-user=opoirion@hawaii.edu

source ~/.bash_profile

""".format(gsm_project, gsm, path_gsm_target))

    ######## new var ################################################
    f_slurm.write('GSM={0}\n'.format(gsm))
    f_slurm.write('GSM_PROJECT={0}\n'.format(gsm_project))
    f_slurm.write('PATH_GSM={0}\n'.format(path_gsm_target))
    f_slurm.write('URL={0}\n'.format(ftp))
    ################################################################
    f_slurm.write('PLATEFORM={0}\n'.format(plateform))
    f_slurm.write('ORGANISM={0}\n'.format(organism))
    f_slurm.write('DBSNP={0}\n'.format(dbsnp))
    f_slurm.write('REF_GENOME={0}\n'.format(ref_genome))
    f_slurm.write('VCF_RESOURCES="{0}"\n'.format(str(vcf_resources).replace(' ', '')))
    f_slurm.write('ANNOTATION_PATH={0}\n'.format(annotation_path))
    f_slurm.write('STAR_INDEX_PATH={0}\n'.format(star_index_path))

    f_slurm.write("""
python2.7 {0}/code/SNV_pipeline/SNV_pipeline/server/gsm_processing_pipeline.py \

       --url $URL \

       --gsm $GSM \

       --gsm_project $GSM_PROJECT \

       --path_gsm $PATH_GSM \

       --star_index_path $STAR_INDEX_PATH \

       --plateform $PLATEFORM \

       --organism $ORGANISM \

       --ref_genome $REF_GENOME \

       --dbsnp $DBSNP \

       --vcf_resources $VCF_RESOURCES \

       --annotation_path $ANNOTATION_PATH
    """.format(CODE_ROOT))
