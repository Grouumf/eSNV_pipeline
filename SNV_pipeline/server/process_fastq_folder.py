from sys import argv

from glob import glob

from SNV_pipeline.server.server_config import ARRAY_LIMIT


assert("--fastqs_folder" in argv)
FASTQS_FOLDER = argv[
    argv.index("--fastqs_folder") + 1]

assert("--organism" in argv)
ORGANISM = argv[
    argv.index("--organism") + 1]

assert("--project_name" in argv)
PROJECT_NAME = argv[
    argv.index("--project_name") + 1]

if "-h" in argv or "--help" in argv:
    print("""
    usage:
    python process_fastq_folder.py ----fastqs_folder FASTQS_FOLDER""" \
          """ --organism ORGANISM --project_name PROJECT_NAME
""")


assert ORGANISM in ['HUMAN', 'MOUSE']


def main():
    """ """
    process_fastqs_folder(FASTQS_FOLDER, ORGANISM, PROJECT_NAME)


def process_fastqs_folder(fastqs_folder,
                          organism,
                          project_name,
                          array_limit=ARRAY_LIMIT):
    """
    example:
    FASTQ='/data/opoirion/10x_data_50_50/fastq/AAACATACGCTGAT-1/AAACATACGCTGAT-1.fastq'
    GSM='AAACATACGCTGAT-1'
    GSM_PROJECT='10x_data_50_50'
    ORGANISM='HUMAN'
    """
    nbatch = 1
    count = 0

    path_file = '{0}/fastq_folder_batch{1}.tsv'.format(fastqs_folder, nbatch)
    f_batch = open(path_file, 'w')

    for fastq_folder in glob('{0}/*'.format(fastqs_folder)):
        regex_fastq = '{0}/*.fastq'.format(fastq_folder)
        gsm = fastq_folder.split('/')[-1]

        if not glob(regex_fastq):
            continue

        f_batch.write('{0}\n'.format('\t'.join([regex_fastq,
                                               gsm,
                                               project_name,
                                                organism])))
        count += 1

        if count >= array_limit:
            print('{0} written'.format(path_file))
            nbatch += 1
            count = 0
            path_file = '{0}/fastq_folder_batch{1}.tsv'.format(
                fastqs_folder, nbatch)
            f_batch = open(path_file, 'w')

    print('{0} written'.format(path_file))


if __name__ == '__main__':
    main()
