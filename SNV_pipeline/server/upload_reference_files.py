from SNV_pipeline.config import TYPE_VAR

from SNV_pipeline.server.server_config import TYPE_VAR as SRV_TYPE_VAR
from SNV_pipeline.server.server_config import HOME_SRV_ADR

from subprocess import check_output

from sys import argv

if '--type' in argv:
    TYPE = argv[argv.index('--type') + 1]
else:
    TYPE = None


def main():
    """ """
    for typ in TYPE_VAR:
        if TYPE and not typ.count(TYPE):
            continue

        print('uploading')
        launch('scp -r {2}:{0} {1}'.format(TYPE_VAR[typ]['ANNOTATION_PATH'],
                                            SRV_TYPE_VAR[typ]['ANNOTATION_PATH'],
                                            HOME_SRV_ADR,
        ))

        for ext in ['.fa', '.fa.fai', '.dict']:
            ref_genome = TYPE_VAR[typ]['REF_GENOME'].rsplit('.', 1)[0] + ext
            srv_ref_genome = SRV_TYPE_VAR[typ]['REF_GENOME'].rsplit('.', 1)[0] + ext

            launch('scp -r {2}:{0} {1}'.format(ref_genome, srv_ref_genome, HOME_SRV_ADR,))

        for ressource, ressource_srv in zip(
                TYPE_VAR[typ]['VCF_RESOURCES'],
                SRV_TYPE_VAR[typ]['VCF_RESOURCES']):
            launch('scp -r {2}:{0} {1}'.format(ressource, ressource_srv, HOME_SRV_ADR,))

        launch('scp -r {2}:{0} {1}'.format(
            TYPE_VAR[typ]['DBSNP'],
            SRV_TYPE_VAR[typ]['DBSNP'], HOME_SRV_ADR))


def launch(cmd):
    print('launching: {0}'.format(cmd))
    check_output(cmd.split())


if __name__ == '__main__':
    main()
