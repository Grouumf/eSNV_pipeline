from SNV_pipeline.server.server_config import ELASTIC_SERVER
from SNV_pipeline.server.server_config import ELASTIC_PORT
from SNV_pipeline.server.server_config import ELASTIC_USER
from SNV_pipeline.server.server_config import ELASTIC_PASSWD

from SNV_pipeline.server.server_config import PATH_LOCAL_DATABASE
from SNV_pipeline.server.server_config import PATH_LOCAL_SOFTFILE

from SNV_pipeline.server.process_soft import extract_gsm_from_soft
from SNV_pipeline.server.process_soft import extract_gse_from_soft

from collections import defaultdict

from glob import glob

from elasticsearch import Elasticsearch

import re

from os.path import split as pathsplit

from sys import stdout

from multiprocessing.pool import Pool


######################## VARIABLE ############################################
ES_CURSOR = Elasticsearch(hosts=ELASTIC_SERVER,
                          port=ELASTIC_PORT,
                          timout=30,
                          http_auth=(ELASTIC_USER, ELASTIC_PASSWD))


NB_SYNC_THREADS = 3
CELL_LIMIT = 500
GENE_LIMIT = 2000
TOP_GENE_PER_CELL = 400
###############################################################################


def main():
    """ """
    # delete_indexes()
    # init_gse_index()
    # find_genes_to_index_and_init_gsm_es_table()
    # synchronize_soft_files()
    synchronize_gene_expr_results()

def delete_indexes():
    """
    """
    try:
        ES_CURSOR.indices.delete(index='gsm')
    except Exception as e:
        print('trying deleting gsm got error: {0}'.format(e))

    try:
        ES_CURSOR.indices.delete(index='gsm_data')
    except Exception as e:
        print('trying deleting gsm_data got error: {0}'.format(e))

    try:
        ES_CURSOR.indices.delete(index='gse')
    except Exception as e:
        print('trying deleting gse got error: {0}'.format(e))

def index_gsm_thread(gene_fpkm):
    """
    """

    es_cursor = Elasticsearch(hosts=ELASTIC_SERVER,
                          port=ELASTIC_PORT,
                          timout=30,
                          http_auth=(ELASTIC_USER, ELASTIC_PASSWD))

    gsm = re.findall('GSM[0-9]+', gene_fpkm)[0]

    gsm_updated = es_cursor.get(index='gsm',
                                doc_type='gsm',
                                id=gsm,
                                _source_include=['gene_fpkm_updated'])

    if gsm_updated['_source']:
        return

    path_gsm = pathsplit(gene_fpkm)[0]
    path_star = '{0}/Log.final.out'.format(path_gsm)
    path_star_sj = '{0}/SJ.out.tab'.format(path_gsm)
    fastqc_path = glob('{0}/*_fastqc'.format(path_gsm))

    if fastqc_path:
        fastqc_status = process_fastqc(fastqc_path[0])
    else:
        fastqc_status = {}

    star_data = process_star_logs(path_star)
    star_sj_data = process_star_splice_junctions(path_star_sj)

    gene_dict = process_gene_fpkm(gene_fpkm)

    top_genes = zip(*sorted(gene_dict.items(), key=lambda x:x[1], reverse=True))

    if top_genes:
        top_genes = top_genes[0][:TOP_GENE_PER_CELL]

    try:
        es_cursor.index('gsm_data',
                         doc_type='gsm',
                         body={},
                         id=gsm)

        es_cursor.update('gsm_data',
                         doc_type='gsm',
                         body={'doc': {
                             'gene_fpkm': gene_dict,
                             'star_splice_junctions': star_sj_data,
                         }
                         },
                         id=gsm)

        es_cursor.update('gsm',
                         doc_type='gsm',
                         body={'doc': {
                             'top_genes': top_genes,
                             'gene_fpkm_updated': True,
                             'fastqc': fastqc_status,
                             'star_logs': star_data,
                         }
                         },
                         id=gsm)
    except Exception as e:
        print('exception found when synchronizing: {0}'.format(e))
        return

    print('#### {0} syncronized'.format(gsm))

def find_genes_to_index_and_init_gsm_es_table(gene_limit=GENE_LIMIT, cell_limit=CELL_LIMIT):
    """
    """
    gene_counter = defaultdict(int)
    gene_mean = defaultdict(float)

    count = 0

    print('indexing genes first pass...')
    gsm_list = glob('{0}/*GSM*/*genes.fpkm_tracking'.format(PATH_LOCAL_DATABASE))

    for gene_fpkm in gsm_list:
        count += 1
        f_gene_fpkm = open(gene_fpkm)
        f_gene_fpkm.readline()

        for line in f_gene_fpkm:
            line = line.split('\t')
            fpkm = float(line[-4])
            gene_short = line[4]

            if not fpkm or gene_short == '-':
                continue

            if gene_short.count(',') or gene_short.count('.'):
                continue

            gene_counter[gene_short] += 1
            gene_mean[gene_short] += fpkm

        if cell_limit and count > cell_limit:
            break

        stdout.write('\r{0} / {1} done'.format(count, len(gsm_list)))
        stdout.flush()

    if gene_limit:
        genes_to_index = zip(*sorted(gene_mean.items(),
                                     key=lambda x:x[1],
                                     reverse=True)[:gene_limit])[0]
    else:
        genes_to_index = []

    init_gsm_data_index(genes_to_index)

def init_gsm_data_index(gene_list):
    """
    """

    body = {
        "settings":{
            "index.mapping.total_fields.limit": GENE_LIMIT + 1000
        },
        "mappings": {
            "gsm": {
                "dynamic": True,
                "properties": {
                    "gene_fpkm":  {
                        "type":     "object",
                        "dynamic": False
                    },
                    "star_splice_junctions.SJdata":  {
                        "type":     "object",
                        "dynamic": False
                    },
                }
            }
        }
    }

    for gene in gene_list:
        body['mappings']['gsm']['properties']['gene_fpkm.{0}'.format(gene)] = {
            "type":     "float"
            }

    ES_CURSOR.indices.create(index='gsm_data', body=body)

def init_gse_index():
    """
    """

    body = {"mappings": {}}

    ES_CURSOR.indices.create(index='gse', body=body)

def process_star_logs(star_log):
    """
    """
    json = {}

    for line in open(star_log):
        if not line.count('|'):
            continue
        line = line.split('|')
        key, value = line
        key = key.strip(' \n')
        value = value.strip(' %\n')

        try:
            value = float(value)
        except Exception:
            pass

        json[key] = value

    return json

def process_gene_fpkm(gene_fpkm):
    """
    """
    gene_dict = {}

    f_gene_fpkm = open(gene_fpkm)
    f_gene_fpkm.readline()

    for line in f_gene_fpkm:
        try:
            line = line.split('\t')
            fpkm = float(line[-4])
            gene_short = line[4]
        except Exception:
            continue

        if not fpkm or gene_short == '-':
            continue

        if gene_short.count(',') or gene_short.count('.'):
            continue

        gene_dict[gene_short] = fpkm

    return gene_dict

def process_fastqc(fastqc_path):
    """
    store STAR splice junction information
    """
    json = {}
    mapping = {'PASS': 1, 'FAIL': 0, 'WARN': 0.5}

    for line in open('{0}/summary.txt'.format(fastqc_path)):
        line = line.split('\t')
        json[line[1]] = mapping[line[0]]

    return json

def process_star_splice_junctions(star_sj_path):
    """
    store STAR splice junction information
    """
    json = {'SJlist':[], 'SJdata':{}}

    for line in open(star_sj_path):
        line = line.split()
        sj_id = '{0}:{1}-{2}'.format(line[0], line[1], line[2])
        sj_data = map(int, line[3:])

        json['SJlist'].append(sj_id)
        json['SJdata'][sj_id] = sj_data

    return json

def synchronize_gene_expr_results():
    """
    """
    query = {
        "_source": "",
        "query": {
            "bool": {"must": [
                {"match": {
                    "gene_fpkm_updated": True
                }}
            ]}
        }
    }

    gsm_already_sync = ES_CURSOR.search(index='gsm',
                                    body=query,
                                    size=10000)['hits']['hits']
    gsm_already_sync = ['{1}{0}/genes.fpkm_tracking'.format(
        gsm['_id'],
        PATH_LOCAL_DATABASE) for gsm in gsm_already_sync]

    print('total number of gsm folder already sync: {0}'.format(len(gsm_already_sync)))

    gsm_list = glob('{0}/*GSM*/*genes.fpkm_tracking'.format(PATH_LOCAL_DATABASE))
    gsm_to_index = set(gsm_list).difference(gsm_already_sync)

    print('total number of gsm folder: {0}'.format(len(gsm_list)))
    print('total number of gsm folder to index: {0}'.format(len(gsm_to_index)))

    pool = Pool(NB_SYNC_THREADS)

    pool.map(index_gsm_thread, gsm_to_index)


def synchronize_soft_files():
    """ """

    query = {
        "query" :{
            "match_all": {}
        }, "_source": ""}

    indexed_gse = set([gse['_id'] for gse in
                       ES_CURSOR.search(index='gse', body=query, size=10000)['hits']['hits']])

    for soft_file in glob('{0}/*.soft'.format(PATH_LOCAL_SOFTFILE)):
        gse_id = re.findall('GSE[0-9]+', soft_file)[0]

        if gse_id in indexed_gse:
            continue

        print('processing {0}...'.format(soft_file))
        gse = extract_gse_from_soft(soft_file,
                                    path_soft='')

        gsm_dict = extract_gsm_from_soft(soft_file,
                                         path_soft='',
                                         remove_not_sra=False,
                                         flatten_gsm=True)
        print('number of samples: {0}'.format(len(gsm_dict)))

        ES_CURSOR.index(index='gse', doc_type='gse', body=gse, id=gse['gse'])

        for gsm in gsm_dict:
            ES_CURSOR.index(index='gsm',
                            doc_type='gsm',
                            body=gsm_dict[gsm],
                            id=gsm)


if __name__ == '__main__':
    main()
