import urllib2

from os.path import isfile
from os import stat

from os import popen

from SNV_pipeline.bash_utils import exec_cmd

from sys import argv

from glob import glob

import re

from time import sleep

from SNV_pipeline.server.server_config import FASTQC
from SNV_pipeline.server.server_config import FASTQ_DUMP

from SNV_pipeline.server.single_cell_client import printf

from urllib2 import URLError

import traceback

import json


################  GLOBAL VARIABLE ##############
if "--url" in argv:
    URL = argv[
        argv.index("--url") + 1]
else:
    URL = None

if "--gsm" in argv:
    GSM = argv[
        argv.index("--gsm") + 1]
else:
    GSM = None

if "--path_gsm" in argv:
    PATH_GSM = argv[
        argv.index("--path_gsm") + 1]
else:
    PATH_GSM = None
################################################


def main():
    """
    """
    download_sra()
    extract_fast_from_sra()


def download_sra(url_address=URL, gsm=GSM, path_to_download=PATH_GSM,
                 verbose=True, printf=printf, stdout=None):
    """ """
    waiting_list = [10, 20, 30]

    f_name = "{0}/{1}.sra".format(path_to_download, gsm)

    if isfile('{0}/download_successfull.log'.format(path_to_download)):
        msg = 'file {0} already downloaded. skipping...'.format(f_name)
        printf(msg)
        return msg
    while True:
        try:
            url = urllib2.urlopen(url_address).read()
        except URLError as e:
            if waiting_list:
                sleep_time = waiting_list.pop()
                printf('error when downloading: {1} sleeping {0} s...'.format(sleep_time, e))
                sleep(sleep_time)
            else:
                raise e
        else:
            break

    srr = re.findall('run=(?P<srr>SRR[0-9]+)', url)[0]

    srr_url = "ftp://ftp-trace.ncbi.nlm.nih.gov"\
              "/sra/sra-instant/reads/ByRun/sra/SRR/{0}/{1}/{1}.sra".format(
                  srr[0:6], srr)

    printf('downloading: {0}'.format(srr_url))

    if verbose:
        verb = ''
    else:
        verb = '--no-verbose'
    cmd = "wget {2} -O {0} {1} ".format(f_name, srr_url, verb)
    printf('launching: {0}'.format(cmd))

    exec_cmd(cmd, stdout)

    msg = '{0} successfully downloaded'.format(f_name)
    printf(msg)

    f_log = open('{0}/download_successfull.log'.format(path_to_download), 'w')
    f_log.write('download complete')

    return msg

def compute_av_read_length(fastq_path, max_read=10000):
    """
    """
    read_lengths = []

    i = 0

    for line in open(fastq_path):

        if line[0] in ['@', '+']:
            continue

        read_length = len(line.strip('\n\t\r').strip())
        read_lengths.append(read_length)

        i += 1

        if i > max_read:
            break

    return median(read_lengths)

def extract_fast_from_sra(gsm=GSM, path_gsm=PATH_GSM, fastq_dump=FASTQ_DUMP,
                          printf=printf, stdout=None):
    """extract sra file"""

    if  isfile('{0}/sra_extract_successfull.log'.format(path_gsm)):
        msg = 'sra for {0} already extracted to fastq. skipping...'.format(gsm)

        with open('{0}/sra_extract_successfull.log'.format(path_gsm)) as f_log:
            read_length = int(float(f_log.readline().strip('\n').split('=')[-1]))
        printf(msg)
        return read_length, msg

    sra_fil = "{0}/{1}.sra".format(path_gsm, gsm)

    assert(isfile(sra_fil))

    printf('go to extraction for file: {0}'.format(sra_fil))
    cmd = '{2} --split-3 -B {0} -O {1}/'\
                 .format(sra_fil, path_gsm, fastq_dump)
    printf('launching: {0}'.format(cmd))
    exec_cmd(cmd, stdout)

    printf('Final memory: after fastq-dump extraction analysis...')
    printf(popen('free -g').read())

    fastq_files = choose_fastq_files(path_gsm, gsm, printf, stdout)

    read_length = compute_av_read_length(fastq_files[0])
    printf('read length inferred: {0}'.format(read_length))

    perform_fastqc_test(path_gsm, fastq_files, printf=printf, stdout=stdout)

    printf('Final memory: after fastqc test analysis...')
    printf(popen('free -g').read())

    f_log = open('{0}/sra_extract_successfull.log'.format(path_gsm), 'w')
    f_log.write('read_length = {0}'.format(read_length))

    msg = 'extraction complete for {0}'.format(gsm)

    return read_length, msg

def choose_fastq_files(path_gsm, gsm, printf, stdout):
    """
    """
    fastq_files = glob('{0}/*.fastq'.format(path_gsm))
    assert(len(fastq_files))

    if len(fastq_files) == 3:
        printf('3 fastq files detected: {0}'.format(fastq_files))

        assert(glob('{0}/*_1.fastq'.format(path_gsm)))
        assert(glob('{0}/*_2.fastq'.format(path_gsm)))
        assert(glob('{0}/*{1}.fastq'.format(path_gsm, gsm)))

        single_fastq = glob('{0}/*{1}.fastq'.format(path_gsm, gsm))[0]
        paired_fastq = glob('{0}/*_1.fastq'.format(path_gsm))[0]

        size_single = stat(single_fastq).st_size
        size_paired = stat(paired_fastq).st_size

        if size_single > size_paired:
            printf('keeping single read fastq file: {0}'.format(single_fastq))
            exec_cmd('rm {0}/{1}_1.fastq'.format(path_gsm, gsm), stdout)
            exec_cmd('rm {0}/{1}_2.fastq'.format(path_gsm, gsm), stdout)
        else:
            printf('keeping paired read fastq files: {0}'.format(paired_fastq))
            exec_cmd('rm {0}/{1}.fastq'.format(path_gsm, gsm), stdout)

        fastq_files = glob('{0}/*.fastq'.format(path_gsm))

    return fastq_files

def perform_fastqc_test(path_gsm, fastq_files, fastqc=FASTQC,
                        printf=printf, stdout=None):
    """
    """
    if  isfile('{0}/fastq_test_successfull.log'.format(path_gsm)):
        printf('fastq test for {0} already performed. skipping...'.format(path_gsm))

    fastq_file = fastq_files[0]

    cmd = '{0} --extract --outdir {1} {2}'.format(
    fastqc, path_gsm, fastq_file)
    printf('launching: {0}'.format(cmd))

    try:
        exec_cmd(cmd, stdout)
    except Exception as e:
        trace = traceback.format_exc()
        printf('fastq test for {0} return an exception: {1}'.format(path_gsm, trace))
        return {}

    summary = open('{0}_fastqc/summary.txt'.format(
        fastq_file.rsplit('.', 1)[0]))

    summary_dict = {}

    for line in summary:
        status, test, fastq = line.strip('\n').split('\t')
        summary_dict[test] = status

    cmd = 'rm {0}_fastqc.html'.format(fastq_file.rsplit('.', 1)[0])
    exec_cmd(cmd, stdout)

    cmd = 'rm {0}_fastqc.zip'.format(fastq_file.rsplit('.', 1)[0])
    exec_cmd(cmd, stdout)
    f_log = open('{0}/fastq_test_successfull.log'.format(path_gsm), 'w')
    f_log.write(json.dumps(summary_dict, indent=2))

    return summary_dict


def median(lst):
    """
    """
    n = len(lst)

    if n < 1:
            return None

    if n % 2 == 1:
            return sorted(lst)[n / 2]
    else:
            return int(sum(sorted(lst)[n/2-1: n/2+1]) / 2.0)


if __name__ == '__main__':
    main()
