from sys import argv

from SNV_pipeline.bash_utils import exec_cmd

from SNV_pipeline.server.server_config import SOFT_FILE_TO_DOWNLOAD
from SNV_pipeline.server.process_soft import extract_gsm_from_soft

from glob import glob

from collections import Counter


USAGE = """
python download_soft_files.py [--soft_id <GSE>] [--path_to_file <file>]
    --id_file    the path toawrd the file containing the GSE ids (one id per line)
    --h     display this help
    --soft_id     [OPTIONAL] The GSE id to download.
"""


def main():
    """ """
    assert len(argv) > 1

    if '--h' in argv:
        print(USAGE)
        return

    if '--soft_id' in argv:
        soft_id = argv[argv.index('--soft_id') + 1]
        download_and_process_soft(soft_id)

    if '--soft_file' in argv:
        soft_file = argv[argv.index('--soft_file') + 1]

        for soft_id in open(soft_file):
            try:
                download_and_process_soft(soft_id.strip('\n\r\t '))
            except Exception as e:
                print('exception found when downloading: {0} {1}'.format(soft_id, e))

def download_and_process_soft(gse, erase=False):
    """
    """
    if not erase:
        if glob('{0}/{1}*'.format(SOFT_FILE_TO_DOWNLOAD, gse)):
            print('soft file seems existing for: {0}'.format(gse))
            return

    print('downloadin: {0}...'.format(gse))

    address = 'ftp://ftp.ncbi.nlm.nih.gov/geo/series/{0}nnn/{1}/soft/{1}_family.soft.gz'.format(
        gse[:-3],
        gse
    )

    exec_cmd('wget {0} -O {1}/{2}.soft.gz'.format(address, SOFT_FILE_TO_DOWNLOAD, gse))
    exec_cmd('gzip -d {1}/{0}.soft.gz'.format(gse, SOFT_FILE_TO_DOWNLOAD))

    rename_soft('{0}.soft'.format(gse), SOFT_FILE_TO_DOWNLOAD)

def rename_soft(soft_file, path_soft):
    """
    """
    gse_dict = extract_gsm_from_soft(soft_file, path_soft=path_soft)

    if not gse_dict:
        print('soft_file:{0} empty!'.format(soft_file))
        return

    n_samples = len(gse_dict)

    organism = Counter([gse_dict[gse]['organism_ch1'][0] for gse in gse_dict])

    organism = sorted(organism.items(), key=lambda x:x[1], reverse=True)[0][0]
    organism = organism.split()[0]

    exec_cmd('mv {0}/{1}.soft {0}/{1}_n{2}_{3}.soft'.format(
        path_soft, soft_file.rsplit('.', 1)[0], n_samples, organism))

    print('{0}/{1}_n{2}_{3}.soft created!'.format(
        path_soft, soft_file.rsplit('.', 1)[0], n_samples, organism))


if __name__ == '__main__':
    main()
