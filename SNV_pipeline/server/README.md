# SNV pipeline (server)

This pipeline is currently deployed on the hawaii HPC super computer, but also in RNA2. It can automatically download, preprocess, align, compute gene expression and infer SNV, using just a GSE ID as a input
. Furthermore, the data once processed arre automatically transfered to RNA2 in the form of a GSM folder (the name of the GSM folder is the ID). These GSM folder can then be synconized with the elastic_cell package [https://gitlab.com/granatum-developer-group/elastic_cell](https://gitlab.com/granatum-developer-group/elastic_cell).  However, the installation is a little bit painfull since it requires a lot of softwares to be also installed (STAR, samtools, bedtools, picard-tools, java, fastqc, cufflinks, GATK, freebayes), in addtition to reference genomes. Fortunatly, the package is already installed with my current user id on the hpc manoa: `opoirion`. (You can contact me for the password). Furthermore, the SNV_pipeline package has to be both installed on the HPC server and the local server (i.e. RNA2) in order to check if one sample is already present into the database to not redownload it again


## Installation

```bash
git clone https://gitlab.com/Grouumf/eSNV_pipeline
cd eSNV_pipeline
pip install -e .
cd eSNV_pipeline/SNV_pipeline
```

## launch test

```bash
nose test/test_snv.py
```

## Principle

* connect on uhhpc (for example opoirion@uhhpc1.its.hawaii.edu you package should be installed here)

* go to lus and create a folder to store the log

```bash
cd lus
mkdir log_file
```

* launch pipeline

```bash
python ~/apps/code/SNV_pipeline/SNV_pipeline/server/launch_slurm_analysis.py --name GSE80232
```
