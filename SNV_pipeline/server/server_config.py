from os.path import abspath
from os.path import split as pathsplit

from sys import argv

# absolute path of this file
PATH_THIS_FILE = pathsplit(abspath(__file__))[0]

################ SERVER VARIABLE ########################################################
PORT = 8085
HOST = '10.0.1.206'
SRV_ADR = 'opoirion@uhhpc1.its.hawaii.edu'
HOME_SRV_ADR = 'opoirion@rna2.cc.hawaii.edu'
ARRAY_LIMIT = 400
MAX_SIMULTANEOUS_ARRAY = 30
#########################################################################################

##################################ELASTIC SEARCH ########################################
ELASTIC_SERVER = '0.0.0.0'
ELASTIC_PORT = 9200
ELASTIC_USER = 'garmirestaff'
ELASTIC_PASSWD = 'garmire'
REMOTE_ELASTIC_SERVER = 'rna2.cc.hawaii.edu'
SSH_USER = 'opoirion'
#########################################################################################

################ SRV ARCHITECTURE #######################################################
PATH_OUTPUT_FOLDER = '/home/opoirion/lus/data/'
SOFT_FILE_TO_DOWNLOAD = '/home/opoirion/lus/data/soft_file/'

GLOBAL_DATA_ROOT = '/home/opoirion/lus/refdata/'
PROJECT_ROOT = '/home/opoirion/lus/refdata/'
OUTPUT_ROOT = '/home/opoirion/lus/'
PROG_ROOT = '/home/opoirion/apps/prog/'
CODE_ROOT = '/home/opoirion/apps/code/'
#####################################PARSER FOR MAIN ARGS  ##############################

################ SQLITE VARIABLE ########################################################
#depreciated
SQLITE_PATH = '{0}/data/'.format(PATH_THIS_FILE)
#########################################################################################

if '--path_output_folder' in argv:
    PATH_OUTPUT_FOLDER = argv[argv.index('--path_output_folder') + 1]

if '--soft_file_to_download' in argv:
    SOFT_FILE_TO_DOWNLOAD = argv[argv.index('--soft_file_to_download') + 1]

if '--global_data_root' in argv:
    GLOBAL_DATA_ROOT = argv[argv.index('--global_data_root') + 1]

if '--project_root' in argv:
    PROJECT_ROOT = argv[argv.index('--project_root') + 1]

if '--output_root' in argv:
    OUTPUT_ROOT = argv[argv.index('--output_root') + 1]

if '--prog_root' in argv:
    PROG_ROOT = argv[argv.index('--prog_root') + 1]

if '--code_root' in argv:
    CODE_ROOT = argv[argv.index('--code_root') + 1]

PATH_GSM_PROJECTS = '{0}/data/'.format(OUTPUT_ROOT)
#########################################################################################

################ LOCAL ARCHITECTURE #####################################################
PATH_LOCAL_DATABASE = '/mnt/nas_rna2/opoirion/single_cell_database/data/'
PATH_LOCAL_SOFTFILE = '/mnt/nas_rna2/opoirion/single_cell_database/soft_file/'
# for fastq_files
PATH_LOCAL_DATA  = '/home/opoirion/data/'
#########################################################################################

TYPE_VAR = {
    'HUMAN': {
        'BOWTIE': "{0}/Illumina_hg19/BowtieIndex/"\
        .format(GLOBAL_DATA_ROOT),
        'BOWTIE2': "{0}/Illumina_hg19//BowtieIndex2/genome"\
        .format(GLOBAL_DATA_ROOT),
        'ANNOTATION_PATH': "{0}/Illumina_hg19/Annotation/genes.gtf"\
        .format(GLOBAL_DATA_ROOT),
        'GENOME_PATH': "{0}/Illumina_hg19/Chromosomes"\
        .format(GLOBAL_DATA_ROOT),
        'STAR_INDEX_PATH': "{0}/Illumina_hg19/Sequences/STARindex"\
        .format(GLOBAL_DATA_ROOT),
        'REF_GENOME': "{0}/Illumina_hg19/Sequences/WholeGenomeFasta/genome.fa"\
        .format(GLOBAL_DATA_ROOT),
        'PATH_CHR': "{0}/Illumina_hg19/Chromosomes/"\
        .format(GLOBAL_DATA_ROOT),
        'ORGANISM': 'hg19',
        'DBSNP': "{0}/Illumina_hg19/vcf/dbsnp_138.hg19.vcf"\
        .format(GLOBAL_DATA_ROOT),
        'VCF_RESOURCES': [
            "{0}/Illumina_hg19/vcf/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf"\
            .format(GLOBAL_DATA_ROOT),
            "{0}/Illumina_hg19/vcf/1000G_phase1.indels.hg19.sites.vcf"\
            .format(GLOBAL_DATA_ROOT),
            ]
    },
    'MOUSE': {
        'BOWTIE': "{0}/Mus_musculus/UCSC/mm10/Sequence/BowtieIndex/"\
        .format(GLOBAL_DATA_ROOT),
        'BOWTIE2': "{0}/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/mm10"\
        .format(GLOBAL_DATA_ROOT),
        'ANNOTATION_PATH': "{0}/Mus_musculus/UCSC/mm10/Annotation/genes.gtf"\
        .format(GLOBAL_DATA_ROOT),
        'GENOME_PATH': "{0}/Mus_musculus/UCSC/mm10/Sequence/Chromosomes"\
        .format(GLOBAL_DATA_ROOT),
        'STAR_INDEX_PATH': "{0}/Mus_musculus/UCSC/mm10/Sequence/STARindex"\
        .format(GLOBAL_DATA_ROOT),
        'PATH_CHR': "{0}/Mus_musculus/UCSC/mm10/Sequence/Chromosomes/"\
        .format(GLOBAL_DATA_ROOT),
        'REF_GENOME': "{0}/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa"\
        .format(GLOBAL_DATA_ROOT),
        'ORGANISM': 'mm10',
        'DBSNP': "{0}/Mus_musculus/vcf/mgp.v3.snps.rsIDdbSNPv137_ordered.vcf"\
        .format(GLOBAL_DATA_ROOT),
        'VCF_RESOURCES': [
            "{0}/Mus_musculus/vcf/mgp.v3.indels.rsIDdbSNPv137_ordered.vcf"\
            .format(GLOBAL_DATA_ROOT)
            ]
    }
}
#####################################################################


############# DATASET ###############################################

SPECIFIC_FILENAME_PATTERN = ""
#####################################################################

############# SOFTWARE ##############################################
JAVA = "{0}/jdk1.8.0_77/bin/java".format(PROG_ROOT)
JAVA_MEM = "-Xmx110g"
GATK_DIR = "{0}/GATK/".format(PROG_ROOT)
PICARD_DIR = "{0}/picard-tools-2.1.1".format(PROG_ROOT)
PATH_HTML = "{0}/d3visualisation/".format(CODE_ROOT) # OPTIONAL
FASTQC = "{0}/FastQC/fastqc".format(PROG_ROOT)
FASTQ_DUMP = "{0}/sratoolkit.2.8.2-1-binary/bin/fastq-dump".format(PROG_ROOT)
CUFFLINKS = 'cufflinks'

SAMTOOLS = '{0}/samtools-1.5/bin/samtools'.format(PROG_ROOT)
#####################################################################

#############  STAR #################################################
PATH_STAR_SOFTWARE = "STAR"

STAR_THREADS = 10
#####################################################################

########### FREEBAYES SNV CALLING PIPELINE ##########################
PATH_OPOSSUM = '{0}/Opossum/'.format(PROG_ROOT)
PATH_FREEBAYES = '{0}/freebayes/bin/freebayes'.format(PROG_ROOT)
####################################################################

############ COMPUTE DISTANCE MATRIX ##########################################
FEATURE_COUNT = "{0}/subread-1.5.0-p1-Linux-x86_64/bin/featureCounts"\
                      .format(PROG_ROOT)
###############################################################################
