from SNV_pipeline.server.server_config import ELASTIC_PORT
from SNV_pipeline.server.server_config import ELASTIC_SERVER
from SNV_pipeline.server.server_config import ELASTIC_USER
from SNV_pipeline.server.server_config import ELASTIC_PASSWD

from elasticsearch import Elasticsearch

from sys import stdout

import json


def collect_remote_synchronised_gse():
    """
    collect all the GSE from the sc database for which
    we have at least one single cells synchronized
    """
    body = {
    "size": 0,
        "query": {"bool": {"must": [
            {"term": {
                "gene_fpkm_updated": {
                    "value": True
                }
            }}
        ]}},
        "aggs": {
            "GSE": {
                "terms": {
                    "field": "series_id.keyword",
                    "size":1000
                }
            }
        }
    }

    esc = Elasticsearch(hosts=ELASTIC_SERVER,
                    port=ELASTIC_PORT,
                    timout=30,
                    http_auth=(ELASTIC_USER, ELASTIC_PASSWD))

    results = esc.search(index='gsm', body=body)['aggregations']['GSE']

    stdout.write(json.dumps(results, indent=2))


if __name__ == '__main__':
    collect_remote_synchronised_gse()
