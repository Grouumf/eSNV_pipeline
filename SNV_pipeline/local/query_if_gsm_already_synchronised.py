from SNV_pipeline.server.server_config import ELASTIC_PORT
from SNV_pipeline.server.server_config import ELASTIC_SERVER
from SNV_pipeline.server.server_config import ELASTIC_USER
from SNV_pipeline.server.server_config import ELASTIC_PASSWD

from elasticsearch import Elasticsearch

from sys import stdout

from sys import  argv

import json


def main():
    """
    """
    query_if_gsm_already_synchronised(argv[1])


def query_if_gsm_already_synchronised(gsm):
    """
    collect all the GSE from the sc database for which
    we have at least one single cells synchronized
    """
    esc = Elasticsearch(hosts=ELASTIC_SERVER,
                    port=ELASTIC_PORT,
                    timout=30,
                    http_auth=(ELASTIC_USER, ELASTIC_PASSWD))

    results = esc.get(index='gsm',
                      doc_type='gsm', id=gsm,
                      _source_include=[
                          'gene_fpkm_updated',
                          'raw_count_updated',
                          'snv_updated'])['_source']

    stdout.write(json.dumps(results, indent=2))


if __name__ == '__main__':
    main()
