#!/usr/bin/python

"""
read soft summary file from GEO page and map GSE to cell name
"""

from SNV_pipeline.config import GLOBAL_DATA_ROOT
from SNV_pipeline.config import PROJECT_NAME

from os.path import isfile

import re

def main():
    soft_path = "{0}/{1}/{1}.soft"\
                .format(GLOBAL_DATA_ROOT, PROJECT_NAME)
    csv_path = "{0}/{1}/{1}.csv"\
               .format(GLOBAL_DATA_ROOT, PROJECT_NAME)

    if not isfile(soft_path):
        print("error! no file: {0}".format(soft_path))
        return 1

    f_soft = open(soft_path, 'r').read()
    f_csv = open(csv_path, 'w')

    gse_list = re.findall("(?<=\^SAMPLE \= )\w+", f_soft)
    id_list = re.findall("Sample_title \= (?P<id>.+)\n", f_soft)
    batch_list = re.findall("(?<=BATCH_ID\=)[0-9]+", f_soft)

    if batch_list:
        try:
            assert(len(batch_list) == len(id_list))
        except Exception:
            print('batch id found but different length from id_list')
        else:
            id_list = map(lambda x: '{0}_batch{1}'.format(x[0], x[1]),
                          zip(id_list, batch_list))

    for gse, ids in zip(gse_list, id_list):
        ids = ids.replace(' ', '_')
        f_csv.write("{0};{1}\n".format(gse, ids))

    print("done")

if __name__ == "__main__":
    main()
