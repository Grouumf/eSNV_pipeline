"""
config variable for SNV pipeline
"""

from SNV_pipeline.config import PROG_ROOT
from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import PROJECT_ROOT

ESNV = "eSNV-Detect_1.0"


def make_config(path):
    """
    create a config file config.txt at the path
    """
    CONFIG_RAW = """
## tools
PICARD={0}/{1}/bin/picard/picard-tools-1.106
SAMTOOLS={0}/{1}/bin/samtools/samtools-0.1.19
GATK={0}/{1}/bin/gatk/GenomeAnalysisTK-1.6-9-g47df7bb
BCFTOOLS={0}/{1}/bin/samtools/samtools-0.1.19/bcftools
TABIX={0}/{1}/bin/tabix/tabix-0.2.6
ANNOVAR={3}/annovar/
ANNOVARDB={3}/annovar/humandb/
SCRIPT_PATH={0}/{1}/src


###execultables
JAVA=/usr/bin
PERL=/usr/bin

### references
REF_GENOME={2}
dbSNP_REF={0}/{1}/resources/dbsnp.vcf.gz
REFSEQ_UNIPORT={0}/{1}/resources/RefSeq_Uniprot_mapping_table.txt
HUMAN_DOMAIN_CORR={0}/{1}/resources/Human_domain_coordinates.txt

### flags and paramters
REALIGNEMNT=YES
RECALIBRATION=YES
ALIGNER=TOPHAT:MAPSPLICE
PROTEOMICS=YES
TEMPORARY_FILES_REMOVE=NO
CHR_INDEX=1:2:3:4:5:6:7:8:9:10:11:12:13:14:15:16:17:18:19:20:21:22:X:Y:M
JVM_MEM=-Xmx56g -Xms5g
NCT=10

### CUSTOMIZED PARAMTERS
HIGH_ReadRankPosSum=8
LOW_ReadRankPosSum=-8
MIN_ALT_READS=3
MIN_READ_DEPTH=3
R_CUTOFF=0.1
R_CUTOFF2=0.05
KEEP_ALL_SNV=T
"""\
    .format(PROG_ROOT,
            ESNV,
            REF_GENOME,
            PROJECT_ROOT)


    with open(path, 'w') as f:
        f.write(CONFIG_RAW)
    return
