# SNV computation pipeline

## installation (local)

```bash
pip install -r requirements.txt --user
```

## configuration
* all local variables must be set into the file config.py

```python
############# DATASET ADENOCARCINO ###################################
PATH = "/data/opoirion/glioblastoma/s2.fastq/"
PATH_OUTPUT = "/home/opoirion/data/glioblastoma/"
SPECIFIC_FILENAME_PATTERN = ""
#####################################################################

############# DATASET ADENOCARCINO ###################################
PATH = "/data/opoirion/pancratic_circulating_tumor/fastq/"
PATH_OUTPUT = "/home/opoirion/data/pancreatic_circulating_tumor/"
SPECIFIC_FILENAME_PATTERN = "*GSM1243735*"
#####################################################################

#############  MAPSPLICE ############################################
OUTPUT_PATH_MAPSPLICE = PATH_OUTPUT + "/mapsplice/"
BOWTIE = "/data/opoirion/Illumina_hg19/BowtieIndex/"
GENOME_PATH = "/data/opoirion/Illumina_hg19/Chromosomes"
PATH_MAPSPLICE_SOFTWARE = "/home/opoirion/prog/MapSplice-v2.1.8/mapsplice.py"
MAPSPLICE_THREADS = 5
#####################################################################

################ TOPHAT #############################################
OUTPUT_PATH_TOPHAT = PATH_OUTPUT + "/tophat/"
BOWTIE = "/data/opoirion/Illumina_hg19//BowtieIndex2/genome"
TOPHAT_THREADS = 5
#####################################################################

############ PROCESS eSNV ###########################################
OUTPUT_PATH_ESNV =  PATH_OUTPUT + '/esnv_raw_result/'
OUTPUT_PATH_ESNV_FINAL = PATH_OUTPUT + '/esnv_result/'
PATH_TO_DATA = PATH_OUTPUT
ESNV_PATH = "/home/opoirion/prog/eSNV-Detect_1.0/"
NB_PROCESS = 2
####################################################################

############ COMPUTE DISTANCE MATRIX ##########################################
PATH_COUNT_SOFTWARE = "/home/opoirion/prog/subread-1.5.0-p1-source/bin/featureCounts"
ANNOTATION_PATH = "/data/opoirion/Illumina_hg19/Annotation/genes.gtf"
MATRIX_OUTPUT_PATH = "/home/opoirion/data/glioblastoma/expression_profile/"
###############################################################################
```

## usage
* call tophat
```bash
python SNV_pipeline/deploy_fastq_aligner/deploy_tophat.py
```
* call mapsplice
```bash
python SNV_pipeline/deploy_fastq_aligner/deploy_masplice.py
```

* compute expression matrix
```bash
python SNV_pipeline/deploy_fastq_aligner/compute_frequency_matrix.py
```

### to be continued...
