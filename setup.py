from setuptools import setup, find_packages
import sys, os

VERSION = '0.23.13'

setup(name='SNV_pipeline',
      version=VERSION,
      description="compute SNV from RNA-seq and produce gene expression matrix",
      long_description="""""",
      classifiers=[],
      keywords='SNV RNA-seq',
      author='o_poiron',
      author_email='o.poirion@gmail.com',
      url='',
      license='MIT',
      packages=find_packages(exclude=['examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      )
