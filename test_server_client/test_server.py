import unittest

from os import popen
from os import listdir

from commands import getstatusoutput

from os.path import isfile
from os.path import isdir
from os.path import split as pathsplit

from SNV_pipeline.config import OUTPUT_ROOT

from SNV_pipeline.config import REF_GENOME
from SNV_pipeline.config import STAR_INDEX_PATH
from SNV_pipeline.config import DBSNP
from SNV_pipeline.config import VCF_RESOURCES
from SNV_pipeline.config import FASTQ_PATH
from SNV_pipeline.config import SPECIFIC_FILENAME_PATTERN as PATTERN

from SNV_pipeline.config import JAVA
from SNV_pipeline.config import GATK_DIR
from SNV_pipeline.config import PICARD_DIR
from SNV_pipeline.config import PATH_STAR_SOFTWARE

PORT = 8085
HOST = '10.0.1.206'
SRV_ADR = 'opoirion@uhhpc1.its.hawaii.edu'
HOME_SRV_ADR = 'opoirion@rna2.cc.hawaii.edu'
ARRAY_LIMIT = 400
MAX_SIMULTANEOUS_ARRAY = 30
ELASTIC_SERVER = '0.0.0.0'
ELASTIC_PORT = 9200
ELASTIC_USER = 'garmirestaff'
ELASTIC_PASSWD = 'garmire'
REMOTE_ELASTIC_SERVER = 'rna2.cc.hawaii.edu'
SSH_USER = 'opoirion'
PATH_OUTPUT_FOLDER = '/home/opoirion/lus/data/'
SOFT_FILE_TO_DOWNLOAD = '/home/opoirion/lus/data/soft_file/'

GLOBAL_DATA_ROOT = '/home/opoirion/lus/refdata/'
PROJECT_ROOT = '/home/opoirion/lus/refdata/'
OUTPUT_ROOT = '/home/opoirion/lus/'
PROG_ROOT = '/home/opoirion/apps/prog/'
CODE_ROOT = '/home/opoirion/apps/code/'


from fnmatch import fnmatch


class TestPackage(unittest.TestCase):
    """ """
    def test_output_root(self):
        """assert that OUTPUT_ROOT folder exists"""
        self.assertTrue(isdir(OUTPUT_ROOT))

    def test_ref_genome(self):
        """assert that ref genome file exists"""
        self.assertTrue(isfile(REF_GENOME))

    def test_annotation_path(self):
        """assert that STAR ref folder exists"""
        self.assertTrue(isdir(pathsplit(STAR_INDEX_PATH)[0]))

    def test_dbsnp(self):
        """assert that dbsnp vcf file exists"""
        self.assertTrue(isfile(DBSNP))

    def test_vcf_resources(self):
        """assert that additional vcf file exist"""
        for vcf in VCF_RESOURCES:
            self.assertTrue(isfile(vcf))

    def test_fastq_path(self):
        """assert that fastq path exists"""
        self.assertTrue(isdir(FASTQ_PATH))

    def test_fastq_path_not_empty(self):
        """assert that fastq path exists"""
        self.assertTrue(len(listdir(FASTQ_PATH)))

    def test_fastq_path_with_folders_with_fastqfile(self):
        """assert that fastq folder exists and that .fastq files are inside"""

        for fastq_folder in listdir(FASTQ_PATH):
            if isfile(FASTQ_PATH + fastq_folder):
                continue
            if PATTERN and not fnmatch(fastq_folder, PATTERN):
                continue

            folder = "{0}/{1}".format(FASTQ_PATH, fastq_folder)

            print('testing if {0} is empty'.format(folder))
            self.assertTrue(filter(lambda fil: fnmatch(fil, '*.fastq'),
                            listdir(folder)))

    def test_java(self):
        """assert that java is installed and > 1.8"""
        res = getstatusoutput('{0} -version'.format(JAVA))[1]
        self.assertIsNotNone(res)

        version = res.split('"')[1].rsplit('.', 1)[0]
        self.assertTrue(float(version) >= 1.8)

    def test_GATK(self):
        """assert that GATK .jar file exists"""
        self.assertTrue(isfile('{0}/GenomeAnalysisTK.jar'.format(GATK_DIR)))

    def test_picard_tools(self):
        """assert that picard-tools .jar files exist"""
        self.assertTrue(isfile('{0}/picard.jar'.format(PICARD_DIR)))
        self.assertTrue(isfile('{0}/picard-lib.jar'.format(PICARD_DIR)))

    def test_STAR_aligner(self):
        """assert that STAR aligner bin exists and return version"""
        self.assertIsNotNone(popen('{0} --version'.format(PATH_STAR_SOFTWARE)))


if __name__ == "__main__":
    unittest.main()
